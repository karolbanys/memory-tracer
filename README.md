# MEMORY TRACER #

### Requirements ###
* Java 8
* Maven 2

### User guide ###
* Build project using Maven
```sh
$ mvn compile package
```
* The above command generates in `target` directory two jars : `memory-tracer-1.0-agent.jar` and `memory-tracer-1.0-runtime.jar`
* Run compiled class with java agent: `memory-tracer-1.0-agent.jar`
* Put on boot classpath runtime jar: `memory-tracer-1.0-runtime.jar`
* Whole command 
```sh
$ java -javaagent:memory-tracer-1.0-agent.jar -Xbootclasspath/a:memory-tracer-1.0-runtime.jar MyClass
```


### Agent arguments ###
Additional arguments for agent are given after `=`, beetween argument and value is `:` and they are separeted by `;`.

Arguments:

* `classToTransform` - string value indicating classes which contain this value will be instrumented. Default ignore.
* `logFilePath` - string value, which indicates a path for log file. Default `TraceLoggerFiles/logs.txt`.
* `logUnknownFromZero` - boolean value, which indicates whether unknown references should be logged from zero. Default true.

Example:
```sh
java -javaagent:memory-tracer-1.0-agent.jar='classToTransform:my/package/MyClass;logFilePath:myPath.txt;logUnknownFromZero:true'
```

More details can be found in documentation.

### Documentation ###
Project is a part of Master Thesis, see [MemoryTracer.pdf](https://bitbucket.org/karolbanys/memory-tracer/src/c7492e29fa0a89eadb87969400b347b24e5eb9e0/MemoryTracer.pdf?at=master&fileviewer=file-view-default).