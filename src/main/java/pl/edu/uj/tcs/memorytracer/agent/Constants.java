package pl.edu.uj.tcs.memorytracer.agent;

/**
 * @author Karol Banyś.
 */
public final class Constants {
    /** Format string for method [class]/[method][desc]*/
    static final String IDENTIFYING_NAME = "%s/%s%s";

    /** Agent arguments */
    static final String CLASS_TO_TRANSFORM = "classToTransform";
    static final String LOG_FILE_PATH = "logFilePath";
    static final String LOG_UNKNOWN_FROM_ZERO = "logUnknownFromZero";
    static final String LOG_UNKNOWN_FROM_ZERO_FALSE = "false";

    /** Save Bytecode of classes */
    static final String[] SAVE_BYTECODE_BEFORE_INSTRUMENTATION_CLASSES = {};
    static final String[] SAVE_BYTECODE_AFTER_INSTRUMENTATION_CLASSES = {};

    /** Multi-level debugging */
    static final Boolean MEMORY_MAPPER_DEBUG_COMPILE = false;
    static final Boolean MEMORY_MAPPER_DEBUG_RUNTIME = false;
    static final Boolean MEMORY_MAPPER_DEBUG_FINALIZE = false;


    /** Names of packages not to instrument */
    static final String[] NOT_INSTRUMENT_PACKAGES = {
            /* Memory Tracer packages */
            "pl/edu/uj/tcs/memorytracer", "pl/edu/uj/tcs/memorytracerruntime", "pl/edu/uj/tcs/archive"
            ,"com/intellij"
            /* Instrumentation connected packages */
            ,"org/objectweb/asm"
            ,"sun/instrument"
            /* Core classes - need to investigate */
            ,"sun/nio"
            ,"java/lang"
    };
}
