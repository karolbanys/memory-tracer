package pl.edu.uj.tcs.memorytracer.agent;

import org.objectweb.asm.*;
import pl.edu.uj.tcs.memorytracerruntime.agent.MemoryTracerAgentRuntime;

import java.util.*;

import static org.objectweb.asm.Type.getMethodDescriptor;

/**
 * @author Karol Banyś.
 */
class MemoryMapperMethodVisitor extends MethodVisitor {

    /**
     * Subclass analyzer adapter to keep types of object's in local variables and stack up to date.
     */
    private final TypesAdapter typesAdapter;

    /** Activation Record Id */
    private int activationRecordIdBigLVIndex;
    private final int activationRecordIdType = Type.LONG;

    /** Local Variables  Map
     * Representation table of local variables.
     * The same as original.
     * For two size types there are two elements.
     */
    //private List<Integer> localVariablesMap;
    private final int localVariablesMapBigLVIndexStart;
    int maxLocals;

    /** Stack Map
     * Representation of stack frame.
     * Each element in list represent one element in normal stack.
     * For two size type there is one element.
     */
    //private List<Integer> stackMap;
    private int stackMapBigLVIndexStart;
    int maxStack;

    //private Map<Integer, Integer> mapLocalToStackAndStackToLocalOfBigLVIndexes;

    /** BigLV
     * Map of local variables table of this frame.
     * In (@link bigLVTypes) there are types of element.
     * For two size types there are two elements.
     * If value is negative it means that reference is uninitialized.
     * {@link Type#VOID} is representation of slot which was made free.
     */
    private List<Integer> bigLVTypes;
    /**
     * For each bigLVIndex counting of elements in maps containing this bigLVIndex.
     */
    private List<Integer> bigLVRefCount;
    private List<Integer> freeOneSize;
    private List<Integer> freeTwoSize;

    private int tempBigLVIndex1;
    private int tempBigLVIndex2;

    /**
     * Labels to global try-finally block.
     */
    private Label startTry;
    private Label endTry;
    private Label startHandleFinally;

    /**
    * Set to store handle catches.
    */
    private HashSet<Label> catchLabels;

    /**
     * Map represent integer to integer mapping.
     * Key is bigLVIndex of uninitialized reference.
     * Value is bigLVIndex of id of uninitialized reference.
     */
    private Map<Integer, Integer> uninitializedObjectIdsBigLVIndexes;
    private int uninitializedThisBigLVIndex;
    private int uninitializedThisIdBigLVIndex;

    /**
     * Set of bigLVIndexes to make log in visitCode method after creation of AR.
     */
    private Set<Integer> bigLVIndexesToLogAfterARCreation;

    private String methodName;

    MethodVisitor next;

    /* --------------------------------------------------- */

    /* ---------BigLV Index--------- */

    private boolean isRefType(int type) {
        return type == Type.OBJECT || type == Type.ARRAY || -type == Type.OBJECT || -type == Type.ARRAY;
    }

    private boolean isTwoSizeType(int type) {
        return type == Type.DOUBLE || type == Type.LONG;
    }

    /**
     * Return size of type.
     * 2 - for long and double.
     * 1 - for the rest of types.
     *
     * @param type type of looking for size.
     * @return size 1 or 2.
     */
    private int typeSize(int type) {
        switch (type) {
            case Type.DOUBLE:
            case Type.LONG:
                return 2;
            default:
                return 1;
        }
    }

    /**
     * Get size of BigLV.
     *
     * @return size of BigLV.
     */
    int getBigLVSize() {
        return bigLVTypes.size();
    }

    /**
     * Initialization of type in BigLV.
     * If necessary, create slot.
     * Set proper type in types array.
     *
     * @param bigLVIndex index in BigLV to create slot for new type.
     * @param typeSize size of type.
     * @param type new type at bigLVIndex.
     */
    private void initBigLVTypes(int bigLVIndex, int typeSize, int type) {
        if (bigLVTypes.size() > bigLVIndex) {
            setBigLVType(bigLVIndex, type);
            if (typeSize == 2) {
                setBigLVType(bigLVIndex + 1, type);
            }
        } else if (bigLVTypes.size() == bigLVIndex) {
            bigLVTypes.add(type);
            if (typeSize == 2) {
                bigLVTypes.add(type);
            }
        } else {
            throw new RuntimeException("Wrong bigLVIndex in init types.");
        }
    }

    /**
     * Return type at bigLVIndex.
     *
     * @param bigLVIndex index in bigLV to check type.
     * @return type in BigLV at bigLVIndex.
     */
    private int getBigLVType(int bigLVIndex) {
        return bigLVTypes.get(bigLVIndex);
    }

    /**
     * Change type under bigLVIndex to new Type.
     *
     * @param bigLVIndex - index in BigLV.
     * @param newType - integer value representing type.
     */
    private void setBigLVType(int bigLVIndex, int newType) {
        bigLVTypes.set(bigLVIndex, newType);
    }

    /**
     * Initialization of Reference Counting in BigLV array.
     *
     * @param bigLVIndex index to be initialized.
     * @param typeSize size of initialized type.
     */
    private void initBigLVRefCount(int bigLVIndex, int typeSize) {
        if (bigLVRefCount.size() > bigLVIndex) {
            bigLVRefCount.set(bigLVIndex, 1);
            if (typeSize == 2) {
                bigLVRefCount.set(bigLVIndex + 1, -1);
            }
        } else if (bigLVRefCount.size() == bigLVIndex) {
            bigLVRefCount.add(1);
            if (typeSize == 2) {
                bigLVRefCount.add(-1);
            }
        } else {
            throw new RuntimeException("Wrong bigLVIndex in init ref count.");
        }

    }

    /**
     * Decrease Reference counting at bigLVIndex.
     *
     * @param bigLVIndex index in BigLV.
     * @return current Reference Counting at bigLVIndex.
     */
    private int decreaseBigLVRefCount(int bigLVIndex) {
        int refCount = bigLVRefCount.get(bigLVIndex);
        if (refCount == 0)
            throw new RuntimeException("BigLVIndex:"+bigLVIndex+" try to decrease refCounting to -1.");
        refCount--;
        bigLVRefCount.set(bigLVIndex, refCount);
        return refCount;
    }

    /**
     * Increase Reference counting at bigLVIndex.
     * @param bigLVIndex index in BigLV.
     * @return current Reference Counting at bigLVIndex.
     */
    private int increaseBigLVRefCount(int bigLVIndex) {
        int refCount = bigLVRefCount.get(bigLVIndex);
        refCount++;
        bigLVRefCount.set(bigLVIndex, refCount);
        return refCount;
    }

    /**
     * Return first empty index in bigLV for given type and initialize it.
     * If possible it returns index which was free.
     * For 2 size types (LONG, DOUBLE) tak 2 places for rest 1 place.
     * Responsibilities:
     * - find empty index
     *
     * @param type type of empty index
     * @return first empty index in bigLV
     */
    private int getFirstEmptyBigLVIndex(int type){
        int firstEmptyBigLVIndex = getBigLVSize();

        int switchType = type;
        if (switchType < 0) {
            switchType = -switchType;
        }

        switch (switchType) {
            case Type.LONG:
            case Type.DOUBLE:
                if (!freeTwoSize.isEmpty()) {
                    firstEmptyBigLVIndex = freeTwoSize.remove(freeTwoSize.size() - 1);
                }
                break;
            default:
                if (!freeOneSize.isEmpty()) {
                    firstEmptyBigLVIndex = freeOneSize.remove(freeOneSize.size() - 1);
                } else if (!freeTwoSize.isEmpty()) {
                    int freeTwoSizeBigLVIndexFirstSlot = freeTwoSize.remove(freeTwoSize.size() - 1);
                    int freeTwoSizeBigLVIndexSecondSlot = freeTwoSizeBigLVIndexFirstSlot + 1;
                    freeOneSize.add(freeTwoSizeBigLVIndexSecondSlot);
                    firstEmptyBigLVIndex = freeTwoSizeBigLVIndexFirstSlot;
                }
        }

        initBigLVIndex(firstEmptyBigLVIndex, type);

        return firstEmptyBigLVIndex;
    }

    /**
     * Initialize bigLVIndex as type.
     *
     * @param bigLVIndex index in bigLV to initialize
     * @param type type to initialize
     */
    private void initBigLVIndex(int bigLVIndex, int type) {
        int switchType = type;
        if (switchType < 0) {
            switchType = -switchType;
        }

        switch (switchType) {
            case Type.LONG:
            case Type.DOUBLE:
                initBigLVTypes(bigLVIndex, 2, type);
                initBigLVRefCount(bigLVIndex, 2);
                break;
            default:
                initBigLVTypes(bigLVIndex, 1, type);
                initBigLVRefCount(bigLVIndex, 1);

        }

        uninitializedObjectIdsBigLVIndexes.remove(bigLVIndex);
    }

    /**
     * Remove element from bigLV.
     * Decrease reference counting.
     * If reference counting is 0:
     * 1. log remove reference - If type is OBJECT or ARRAY.
     * 2. free index if freeSlot is true.
     *
     * @param bigLVIndex index in bigLV to free
     * @param freeSlot flag if free slot
     * @param logEnabled flag if log is enabled
     */
    private void removeElementFromBigLVIndex(int bigLVIndex, boolean freeSlot, boolean logEnabled) {

        if (decreaseBigLVRefCount(bigLVIndex) > 0) {
            return;
        }

        int type = getBigLVType(bigLVIndex);

        int switchType = type;
        if (switchType < 0)
            switchType = -type;

        switch (switchType) {
            case Type.DOUBLE:
            case Type.LONG:
                if (freeSlot) {
                    freeTwoSize.add(bigLVIndex);
                }
                setBigLVType(bigLVIndex, Type.VOID);
                setBigLVType(bigLVIndex+1, Type.VOID);
                break;
            case Type.OBJECT:
            case Type.ARRAY:
                if (logEnabled) {
                    makeLogBigLVIndexAsRemovedReference(bigLVIndex);
                }
            default:
                if (freeSlot) {
                    freeOneSize.add(bigLVIndex);
                }
                setBigLVType(bigLVIndex, Type.VOID);

        }

        if(uninitializedObjectIdsBigLVIndexes.containsKey(bigLVIndex)) {
            int idBigLVIndex = uninitializedObjectIdsBigLVIndexes.get(bigLVIndex);
            uninitializedObjectIdsBigLVIndexes.remove(bigLVIndex);
            removeElementFromBigLVIndex(idBigLVIndex, true, false);
        }
    }

    /**
     * Clone value from bigLVIndex to cloneValueBetweenBigLVIndexes.
     * Rewrite ids for uninitialized objects.
     * If log new edge if log is enabled.
     *
     * @param fromBigLVIndex source
     * @param toBigLVIndex target
     * @param logEnabled flag if log new value
     */
    private void cloneValueBetweenBigLVIndexes(int fromBigLVIndex, int toBigLVIndex, boolean logEnabled) {
        int type = getBigLVType(fromBigLVIndex);
        initBigLVIndex(toBigLVIndex, type);
        int opcodeLoad = getOpcodeLoadInstruction(type);
        super.visitVarInsn(opcodeLoad, fromBigLVIndex);
        int opcodeStore = getOpcodeStoreInstruction(type);
        super.visitVarInsn(opcodeStore, toBigLVIndex);
        if (type == -Type.OBJECT || type == -Type.ARRAY) {
            int fromBigLVIdIndex = uninitializedObjectIdsBigLVIndexes.get(fromBigLVIndex);
            uninitializedObjectIdsBigLVIndexes.put(toBigLVIndex, fromBigLVIdIndex);
            increaseBigLVRefCount(fromBigLVIdIndex);
        }
        if (logEnabled && isRefType(type)) {
            makeLogBigLVIndexAsAddedReference(toBigLVIndex);
        }
    }

    /**
     * Move value from bigLVIndex to cloneValueBetweenBigLVIndexes.
     * Move ids for uninitialized objects.
     *
     * @param fromBigLVIndex source
     * @param toBigLVIndex target
     */
    private void moveValueBetweenBigLVIndexes(int fromBigLVIndex, int toBigLVIndex) {
        int type = getBigLVType(fromBigLVIndex);
        initBigLVIndex(toBigLVIndex, type);
        int opcodeLoad = getOpcodeLoadInstruction(type);
        super.visitVarInsn(opcodeLoad, fromBigLVIndex);
        int opcodeStore = getOpcodeStoreInstruction(type);
        super.visitVarInsn(opcodeStore, toBigLVIndex);
        if (type == -Type.OBJECT || type == -Type.ARRAY) {
            int fromBigLVIdIndex = uninitializedObjectIdsBigLVIndexes.get(fromBigLVIndex);
            uninitializedObjectIdsBigLVIndexes.put(toBigLVIndex, fromBigLVIdIndex);
            uninitializedObjectIdsBigLVIndexes.remove(fromBigLVIdIndex);
        }
    }

    /**
     * Push given map index on operand stack.
     *
     * @param bigLVIndex index in bigLV to push
     */
    private void pushOnOperandStack(int bigLVIndex) {
        int type = getBigLVType(bigLVIndex);
        int opcode = getOpcodeLoadInstruction(type);
        super.visitVarInsn(opcode, bigLVIndex);
    }

    /**
     * Pop from operand stack and store in bigLV at bigLVIndex.
     *
     * @param bigLVIndex index to store value from operand stack.
     * @param logEnabled flag enabling logging
     */
    private void popFromOperandStackAndStoreAt(int bigLVIndex, boolean logEnabled){
        int type = getBigLVType(bigLVIndex);
        int opcode = getOpcodeStoreInstruction(type);
        super.visitVarInsn(opcode, bigLVIndex);
        if (logEnabled) {
            if (type == Type.OBJECT || type == Type.ARRAY) {
                makeLogBigLVIndexAsAddedReference(bigLVIndex);
            } else if (type == -Type.OBJECT || type == -Type.ARRAY) {
                createIdForUninitializedReference(bigLVIndex);
            }
        }
    }

    /* ---------Logging--------- */

    private void createActivationRecord() {
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "createNewActivationRecordRT", getMethodDescriptor(Type.LONG_TYPE), false);
        popFromOperandStackAndStoreAt(activationRecordIdBigLVIndex, false);
    }

    private void logTopOfOperandStackAsAddedToPreviousActivationRecord() {
        super.visitInsn(Opcodes.DUP);
        putActivationRecordIdOnStack();
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "logAddedToPreviousActivationRecordRT",
                getMethodDescriptor(Type.VOID_TYPE, Type.getType(Object.class), Type.LONG_TYPE), false);
    }

    private void deleteActivationRecord() {
        putActivationRecordIdOnStack();
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "deleteActivationRecordRT", getMethodDescriptor(Type.VOID_TYPE, Type.LONG_TYPE), false);
    }

    private void putActivationRecordIdOnStack() {
        super.visitVarInsn(Opcodes.LLOAD, activationRecordIdBigLVIndex);
    }

    private void logAddedReferenceOO() {
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "addedReferenceRT",
                Type.getMethodDescriptor(Type.VOID_TYPE, Type.getType(Object.class), Type.getType(Object.class)),
                false);
    }

    private void logAddedReferenceLO() {
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "addedReferenceRT",
                Type.getMethodDescriptor(Type.VOID_TYPE, Type.LONG_TYPE, Type.getType(Object.class)), false);
    }

    private void logAddedReferenceOL() {
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "addedReferenceRT",
                Type.getMethodDescriptor(Type.VOID_TYPE, Type.getType(Object.class), Type.LONG_TYPE), false);
    }

    private void logAddedReferenceLL() {
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "addedReferenceRT",
                Type.getMethodDescriptor(Type.VOID_TYPE, Type.LONG_TYPE, Type.LONG_TYPE), false);
    }

    private void logRemovedReferenceOO() {
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "removedReferenceRT",
                Type.getMethodDescriptor(Type.VOID_TYPE, Type.getType(Object.class), Type.getType(Object.class)),
                false);
    }

    private void logRemovedReferenceLO() {
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "removedReferenceRT",
                Type.getMethodDescriptor(Type.VOID_TYPE, Type.LONG_TYPE, Type.getType(Object.class)), false);
    }

    private void logRemovedReferenceOL() {
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "removedReferenceRT",
                Type.getMethodDescriptor(Type.VOID_TYPE, Type.getType(Object.class), Type.LONG_TYPE), false);
    }

    private void logRemovedReferenceLL() {
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "removedReferenceRT",
                Type.getMethodDescriptor(Type.VOID_TYPE, Type.LONG_TYPE, Type.LONG_TYPE), false);
    }

    private void logAddedRootSetReference() {
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "addedRootSetReferenceRT", Type.getMethodDescriptor(Type.VOID_TYPE, Type.getType(Object.class)), false);
    }

    private void logRemovedRootSetReference() {
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "removedRootSetReferenceRT", Type.getMethodDescriptor(Type.VOID_TYPE, Type.getType(Object.class)), false);
    }

    private void makeLogBigLVIndexAsAddedReference(int bigLVIndex) {
        if (uninitializedObjectIdsBigLVIndexes.containsKey(bigLVIndex)){
            Integer unInitBigLV = uninitializedObjectIdsBigLVIndexes.get(bigLVIndex);
            pushOnOperandStack(unInitBigLV);
            putActivationRecordIdOnStack();
            logAddedReferenceLL();
        } else {
            //to
            pushOnOperandStack(bigLVIndex);
            //from
            putActivationRecordIdOnStack();
            //MemoryTracerAgent.removedReference(to, from)
            if (Type.LONG == getBigLVType(bigLVIndex)) {
                logAddedReferenceLL();
            } else {
                logAddedReferenceOL();
            }

        }
    }

    private void makeLogBigLVIndexAsRemovedReference(int bigLVIndex) {
        if (uninitializedObjectIdsBigLVIndexes.containsKey(bigLVIndex)){
            Integer unInitBigLV = uninitializedObjectIdsBigLVIndexes.get(bigLVIndex);
            pushOnOperandStack(unInitBigLV);
            putActivationRecordIdOnStack();
            logRemovedReferenceLL();
        } else {
            //to
            pushOnOperandStack(bigLVIndex);
            //from
            putActivationRecordIdOnStack();
            //MemoryTracerAgent.addedReference(to, from)
            if (Type.LONG == getBigLVType(bigLVIndex)) {
                logRemovedReferenceLL();
            } else {
                logRemovedReferenceOL();
            }
        }
    }

    private void makeLogBetweenBigLVIndexAsAddedReference(int fromBigLVIndex, int toBigLVIndex) {
        int fromType = getBigLVType(fromBigLVIndex);
        int toType = getBigLVType(toBigLVIndex);
        int fromLogBigLVIndex = fromBigLVIndex;
        int toLogBigLVIndex = toBigLVIndex;
        if (fromType < 0) {
            fromLogBigLVIndex = uninitializedObjectIdsBigLVIndexes.get(fromBigLVIndex);
        }
        if (toType < 0) {
            toLogBigLVIndex = uninitializedObjectIdsBigLVIndexes.get(toBigLVIndex);
        }
        pushOnOperandStack(toLogBigLVIndex);
        pushOnOperandStack(fromLogBigLVIndex);
        if (Type.LONG == getBigLVType(toLogBigLVIndex)) {
            if (Type.LONG == getBigLVType(fromLogBigLVIndex)) {
                logAddedReferenceLL();
            } else {
                logAddedReferenceLO();
            }
        } else {
            if (Type.LONG == getBigLVType(fromLogBigLVIndex)) {
                logAddedReferenceOL();
            } else {
                logAddedReferenceOO();
            }
        }
    }

    private void makeLogBetweenBigLVIndexAsRemovedReference(int fromBigLVIndex, int toBigLVIndex) {
        int fromType = getBigLVType(fromBigLVIndex);
        int toType = getBigLVType(toBigLVIndex);
        int fromLogBigLVIndex = fromBigLVIndex;
        int toLogBigLVIndex = toBigLVIndex;
        if (fromType < 0) {
            fromLogBigLVIndex = uninitializedObjectIdsBigLVIndexes.get(fromBigLVIndex);
        }
        if (toType < 0) {
            toLogBigLVIndex = uninitializedObjectIdsBigLVIndexes.get(toBigLVIndex);
        }
        pushOnOperandStack(toLogBigLVIndex);
        pushOnOperandStack(fromLogBigLVIndex);
        if (Type.LONG == getBigLVType(toLogBigLVIndex)) {
            if (Type.LONG == getBigLVType(fromLogBigLVIndex)) {
                logRemovedReferenceLL();
            } else {
                logRemovedReferenceLO();
            }
        } else {
            if (Type.LONG == getBigLVType(fromLogBigLVIndex)) {
                logRemovedReferenceOL();
            } else {
                logRemovedReferenceOO();
            }
        }
    }

    private void logReturnedValueAsAddedReference(int type) {
        if (!isRefType(type))
            return;
        super.visitInsn(Opcodes.DUP);
        putActivationRecordIdOnStack();
        logAddedReferenceOL();
    }

    /**
     * Log references on stack map as removed.
     *
     * @param nElementsWithoutLogging number of elements without logging of removing.
     * */
    private void logRemoveAllStackMap(int nElementsWithoutLogging) {
        int stackMapIndex = 0;
        while (stackMapIndex < getStackMapSize()) {
            int stackBigLVIndex = getBigLVIndexOfStackMapIndex(stackMapIndex);
            if (isRefType(getBigLVType(stackBigLVIndex))) {
                makeLogBigLVIndexAsRemovedReference(stackBigLVIndex);
            }
            stackMapIndex = getNextStackMapIndex(stackMapIndex);
        }
    }

    /**
     * Log returned value if method is not instrumented.
     *
     * @param type type of returned value.
     * @param owner owner of invoked method.
     * @param name name of invoked method.
     * @param desc descriptor of invoked method.
     */
    private void logReturningFromNotInstrumented(int type, String owner, String name, String desc) {
        if (!isRefType(type)) {
            return;
        }
        super.visitLdcInsn(owner);
        super.visitLdcInsn(name);
        super.visitLdcInsn(desc);
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "isMethodInstrumentedRT", Type.getMethodDescriptor(Type.BOOLEAN_TYPE, Type.getType("Ljava/lang/String;"),
                Type.getType("Ljava/lang/String;"), Type.getType("Ljava/lang/String;")), false);
        Label ifEnd = new Label();
        super.visitJumpInsn(Opcodes.IFNE, ifEnd);
            logReturnValue();
        super.visitLabel(ifEnd);
    }

    private void logReturnValue() {
        super.visitInsn(Opcodes.DUP);
        putActivationRecordIdOnStack();
        logAddedReferenceOL();
    }

    private void debugInfo(String s){
        super.visitLdcInsn(s);
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "debugInfoRT", Type.getMethodDescriptor(Type.VOID_TYPE, Type.getType("Ljava/lang/String;")), false);
    }

    /* ---------Uninitialized---------- */

    private void addUninitializedMapping(int bigLVIndex, int idBigLVIndex) {
        uninitializedObjectIdsBigLVIndexes.put(bigLVIndex, idBigLVIndex);
    }

    private int getNextIdAndSaveAt() {
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "getNextIdRT", Type.getMethodDescriptor(Type.LONG_TYPE), false);
        int bigLVIndex = getFirstEmptyBigLVIndex(Type.LONG);
        popFromOperandStackAndStoreAt(bigLVIndex, false);
        return bigLVIndex;
    }

    private void connectIdWithReference(int bigLVIndex) {
        pushOnOperandStack(bigLVIndex);
        pushOnOperandStack(uninitializedObjectIdsBigLVIndexes.get(bigLVIndex));
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "connectIdWithReferenceRT",
                Type.getMethodDescriptor(Type.VOID_TYPE, Type.getType(Object.class), Type.LONG_TYPE), false);
    }

    private int getUninitializedThisIdAndSaveAt() {
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "getUninitializedThisIdRT", Type.getMethodDescriptor(Type.LONG_TYPE), false);
        int bigLVIndex = getFirstEmptyBigLVIndex(Type.LONG);
        popFromOperandStackAndStoreAt(bigLVIndex, false);
        return bigLVIndex;
    }

    /**
     * Handle uninitialized references.
     * Ask pl.edu.uj.tcs.agent for next id and store it.
     * Add mapping from bigLVIndex of reference to id bigLVIndex.
     * Log adding edge from Activation Record to this reference.
     *
     * @param bigLVIndex reference BigLV index
     * @return id BigLV index
     */
    private int createIdForUninitializedReference(int bigLVIndex) {
        int idBigLVIndex = getNextIdAndSaveAt();
        addUninitializedMapping(bigLVIndex, idBigLVIndex);
        makeLogBigLVIndexAsAddedReference(idBigLVIndex);
        return idBigLVIndex;
    }

    /**
     * Handle uninitialized references.
     * Ask pl.edu.uj.tcs.agent for uninitialized id and store it.
     * Add mapping from bigLVIndex of reference to id bigLVIndex.
     * Log adding edge from Activation Record to this reference.
     *
     * @param bigLVIndex reference BigLV index.
     * @return id BigLV index.
     */
    private int getIdForUninitializedThis(int bigLVIndex) {
        int idBigLVIndex = getUninitializedThisIdAndSaveAt();
        addUninitializedMapping(bigLVIndex, idBigLVIndex);
        makeLogBigLVIndexAsAddedReference(idBigLVIndex);
        return idBigLVIndex;
    }

    private void changeCurrentUninitializedThis(int bigLVIndex) {
        int idBigLVIndex = uninitializedObjectIdsBigLVIndexes.get(bigLVIndex);
        pushOnOperandStack(idBigLVIndex);
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "changeCurrentUninitializedThisRT", Type.getMethodDescriptor(Type.VOID_TYPE, Type.LONG_TYPE), false);
    }

    private void resetCurrentUninitializedThis() {
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                "resetCurrentUninitializedThisRT", Type.getMethodDescriptor(Type.VOID_TYPE), false);
    }

    /* ---------Local Variables Map--------- */

    /**
     * Get BigLVIndex of Local Variables from index n.
     *
     * @param n index in Local Variables.
     * @return BigLVIndex of Local Variables at n.
     */
    private int getBigLVIndexOfLocalVariablesMap(int n) {
        return localVariablesMapBigLVIndexStart + n;
    }

    /**
     * Add new element to local variables table.
     *
     * @param type type of new element
     * @return index of new element in bigLV.
     */
    private int addNewElementToLocalVariablesMap(int type) {
        return getFirstEmptyBigLVIndex(type);
    }

    /**
     * Store top of mapped stack into local variables map at index n.
     *
     * @param n index in local variables map to store.
     * @param type type of variable.
     */
    private void popFromMappedStackAndStoreIntoLocalVariablesMap(int n, int type) {
        int stackBigLVIndex = getBigLVIndexOfStackMapTop(0);
        int lvBigLVIndex = getBigLVIndexOfLocalVariablesMap(n);
        if (typesAdapter.locals.size() > n && typesAdapter.isObject(typesAdapter.locals.get(n))) {
            if (isRefType(getBigLVType(lvBigLVIndex))) {
                makeLogBigLVIndexAsRemovedReference(lvBigLVIndex);
            } else {
                setBigLVType(lvBigLVIndex, Type.OBJECT);
                makeLogBigLVIndexAsRemovedReference(lvBigLVIndex);
            }
        }
        cloneValueBetweenBigLVIndexes(stackBigLVIndex, lvBigLVIndex, false);
        removeElementFromBigLVIndex(stackBigLVIndex, false, false);
    }

    /**
     * Load from n-th local variables element and push on operand stack.
     * @param n index of element in local variables map to load.
     */
    private void loadFromLocalVariablesMapAndPushOnOperandStack(int n) {
        int bigLVIndex = getBigLVIndexOfLocalVariablesMap(n);
        pushOnOperandStack(bigLVIndex);
    }

    /**
     * Create copy of value under bigLVIndex of n-th local variables slot and save this copy in stackMap.
     * If value exist in stackMap take bigLVIndex of this value and increase reference counting.
     *
     * @param n index in local variables map to load.
     */
    private void loadFromMappedLocalVariablesAndPushOnMappedStack(int n) {
        int lvBigLVIndex = getBigLVIndexOfLocalVariablesMap(n);
        int stackBigLVIndex = getBigLVIndexOfStackMapTop(0);
        cloneValueBetweenBigLVIndexes(lvBigLVIndex, stackBigLVIndex, true);
    }

    /* ---------Stack Map--------- */

    /**
     * Get size of mapped stack.
     *
     * @return size of mapped stack.
     */
    private int getStackMapSize() {
        return typesAdapter.stack.size();
    }

    /**
     * Get number of elements in mapped stack.
     * Method counts item of size 2 as one element.
     *
     * @return number of elements in mapped stack.
     */
    private int getStackMapElementsCount() {
        return typesAdapter.getTruncatedStack().size();
    }

    /**
     * Get previous index in stack.
     *
     * @param currentIndex index in stack from which previous index should be returned.
     * @return previous index in stack.
     */
    private int getPrevStackMapIndex(int currentIndex) {
        return typesAdapter.prevIndexInStack(currentIndex);
    }

    /**
     * Get next index in stack.
     *
     * @param currentIndex index in stack from which next index should be returned.
     * @return next index in stack.
     */
    private int getNextStackMapIndex(int currentIndex) {
        return typesAdapter.nextIndexInStack(currentIndex);
    }

    /**
     * Get index in map stack of n-th element from top0.
     *
     * @param n number of element from top of map stack
     * @return index in map stack
     */
    private int getStackMapIndexOfStackMapTop(int n) {
        return typesAdapter.getIndexInStackOfTop(n);
    }

    /**
     * Get n-th top value.
     *
     * @param n number of elements from top value (0, 1, ...).
     * @return bigLVIndex of n-th top value.
     */
    private int getBigLVIndexOfStackMapTop(int n) {
        return stackMapBigLVIndexStart + getStackMapIndexOfStackMapTop(n);
    }

    private int getBigLVIndexOfStackMapIndex(int stackIndex) {
        return stackMapBigLVIndexStart + stackIndex;
    }

    /**
     * Add new element to stack map.
     *
     * @param type type of new element.
     * @return index of added element in bigLV.
     */
    private int addNewElementToStackMap(int type){
        return getFirstEmptyBigLVIndex(type);
    }

    /**
     * Pop top value from stack map.
     * Log removed value.
     */
    private void popFromMappedStack() {
        int bigLVIndex = getBigLVIndexOfStackMapTop(0);
        removeElementFromBigLVIndex(bigLVIndex, false, true);
    }

    /**
     * Pop top value from stack map.
     * Do not log removed value.
     */
    private void popFromMappedStackWithoutLog() {
        int bigLVIndex = getBigLVIndexOfStackMapTop(0);
        removeElementFromBigLVIndex(bigLVIndex, false, false);
    }

    /**
     * Pop 2 top values from stack map.
     * Log removed values.
     */
    private void pop2FromMappedStack() {
        int bigLVIndexTop0 = getBigLVIndexOfStackMapTop(0);
        removeElementFromBigLVIndex(bigLVIndexTop0, false, true);
        int bigLVIndexTop1 = getBigLVIndexOfStackMapTop(1);
        removeElementFromBigLVIndex(bigLVIndexTop1, false, true);
    }

    /**
     * Pop N top values from stack map.
     * Log removed values.
     *
     * @param n number of values to pop (1, 2, ...).
     */
    private void popNElementsFromMappedStack(int n) {
        int stackMapIndex = getStackMapIndexOfStackMapTop(n-1);
        for(int i=0; i<n; i++) {
            removeElementFromBigLVIndex(getBigLVIndexOfStackMapIndex(stackMapIndex), false, true);
            stackMapIndex = getNextStackMapIndex(stackMapIndex);
        }

    }

    /**
     * Remove top value from map stack and push on operand stack.
     * Log removed value.
     */
    private void popFromMappedStackAndPushOnOperandStack() {
        int bigLVIndex = getBigLVIndexOfStackMapTop(0);
        pushOnOperandStack(bigLVIndex);
        removeElementFromBigLVIndex(bigLVIndex, false, true);
    }

    /**
     * Remove 2 top value from map stack and push on operand stack.
     * Log removed values.
     */
    private void pop2FromMappedStackAndPushOnOperandStack() {
        int bigLVIndexTop0 = getBigLVIndexOfStackMapTop(0);
        int bigLVIndexTop1 = getBigLVIndexOfStackMapTop(1);
        pushOnOperandStack(bigLVIndexTop1);
        pushOnOperandStack(bigLVIndexTop0);
        removeElementFromBigLVIndex(bigLVIndexTop0, false, true);
        removeElementFromBigLVIndex(bigLVIndexTop1, false, true);
    }

    /**
     * Get top from stack map and push on operand stack.
     */
    private void getFromMappedStackAndPushOnOperandStack() {
        int bigLVIndex = getBigLVIndexOfStackMapTop(0);
        pushOnOperandStack(bigLVIndex);
    }

    /**
     * Get 2 top from stack map and push on operand stack.
     */
    private void get2FromMappedStackAndPushOnOperandStack() {
        int bigLVIndexTop1 = getBigLVIndexOfStackMapTop(1);
        pushOnOperandStack(bigLVIndexTop1);
        int bigLVIndexTop0 = getBigLVIndexOfStackMapTop(0);
        pushOnOperandStack(bigLVIndexTop0);
    }

    /**
     * Get n-th top element from map stack and push on operand stack.
     *
     * @param n number of element to load. 0 is top, (0, 1, ...).
     */
    private void getElementFromTopOfMappedStackAndPushOnOperandStack(int n) {
        int bigLVIndex = getBigLVIndexOfStackMapTop(n);
        pushOnOperandStack(bigLVIndex);
    }

    /**
     * Get first n-th elements from map stack and push on operand stack.
     *
     * @param n number of elements to load. (1, 2, ...).
     */
    private void getNElementsFromMappedStackAndPushOnOperandStack(int n) {
        int stackMapIndex = getStackMapIndexOfStackMapTop(n-1);
        for(int i=0; i<n; i++) {
            int bigLVIndex = getBigLVIndexOfStackMapIndex(stackMapIndex);
            pushOnOperandStack(bigLVIndex);
            stackMapIndex = getNextStackMapIndex(stackMapIndex);
        }
    }

    /**
     * Pop top value from operand stack and store it in stack map at index top0.
     *
     * @param type type of pop/pushed value.
     * @param logEnabled flag if log operation.
     */
    private void popFromOperandStackAndPushOnTop0OfMappedStack(int type, boolean logEnabled) {
        int bigLVIndex = getBigLVIndexOfStackMapTop(0);
        initBigLVIndex(bigLVIndex, type);
        popFromOperandStackAndStoreAt(bigLVIndex, logEnabled);
    }

    /**
     * Pop top value from operand stack and store it in pointed index in stack map.
     * @param type type of pop/push value.
     * @param logEnabled flag if log operation.
     * @param stackMapIndex index in stack map to push value.
     */
    private void popFromOperandStackAndPushToPointIndexOnMappedStack(int type, boolean logEnabled, int stackMapIndex) {
        int bigLVIndex = getBigLVIndexOfStackMapIndex(stackMapIndex);
        initBigLVIndex(bigLVIndex, type);
        popFromOperandStackAndStoreAt(bigLVIndex, logEnabled);
    }

    /**
     * Pop top value from operand stack and store it in topN place in stack map.
     *
     * @param type type of pop/pushed value.
     * @param logEnabled flag if log operation.
     * @param nTop number element from top to push value (0, 1, ...).
     */
    private void popFromOperandStackAndPushOnMappedStackOfTop(int type, boolean logEnabled, int nTop) {
        int bigLVIndex = getBigLVIndexOfStackMapTop(nTop);
        initBigLVIndex(bigLVIndex, type);
        popFromOperandStackAndStoreAt(bigLVIndex, logEnabled);
    }

    /**
     * Swap top0 with top1.
     */
    private void swapElementsOnMappedStack() {
        int bigLVIndexTop0 = getBigLVIndexOfStackMapTop(0);
        int bigLVIndexTop1 = getBigLVIndexOfStackMapTop(1);
        int top0Type = getBigLVType(bigLVIndexTop0);
        int top1Type = getBigLVType(bigLVIndexTop1);
        pushOnOperandStack(bigLVIndexTop1);
        pushOnOperandStack(bigLVIndexTop0);
        popFromOperandStackAndPushOnMappedStackOfTop(top0Type, false, 1);
        popFromOperandStackAndPushOnMappedStackOfTop(top1Type, false, 0);

    }

    /**
     * Make duplication of top of stack.
     *
     * @param dupNumber number of duplication: 1 or 2.
     * @param dupDepth depth to put duplicated value(s).
     */
    private void dupOnMappedStack(int dupNumber, int dupDepth) {
        int stackMapIndex = getStackMapIndexOfStackMapTop(0);
        while(stackMapIndex + dupDepth >= getStackMapSize()) {
            int stackBigLVIndex = getBigLVIndexOfStackMapIndex(stackMapIndex);
            moveValueBetweenBigLVIndexes(stackBigLVIndex, stackBigLVIndex+dupNumber);
            if (stackMapIndex+dupDepth == getStackMapSize())
                break;
            stackMapIndex = getPrevStackMapIndex(stackMapIndex);
        }

        int endCopy = stackMapIndex+dupNumber;
        while (stackMapIndex < endCopy) {
            int stackBigLVIndex = getBigLVIndexOfStackMapIndex(stackMapIndex);
            cloneValueBetweenBigLVIndexes(stackBigLVIndex+dupDepth, stackBigLVIndex, true);
            stackMapIndex = stackMapIndex + (isTwoSizeType(getBigLVType(stackBigLVIndex)) ? 2 : 1);//getNextStackMapIndex(stackMapIndex);
        }
    }


    /* -------- Temp ----------- */

    /**
     * Pop value from operand stack and push into temp.
     *
     * @param type type of pop/pushed value.
     * @param logEnabled flag if log operation.
     */
    private void popFromOperandStackAndPushOnTemp(int type, boolean logEnabled) {
        initBigLVIndex(tempBigLVIndex1, type);
        popFromOperandStackAndStoreAt(tempBigLVIndex1, logEnabled);
    }

    /**
     * Pop from temp and push on stack map.
     * Do not log operation.
     */
    private void popFromTempAndPushOnMappedStackWithoutLog() {
        pushOnOperandStack(tempBigLVIndex1);
        int bigLVIndex = getBigLVIndexOfStackMapTop(0);
        initBigLVIndex(bigLVIndex, getBigLVType(tempBigLVIndex1));
        popFromOperandStackAndStoreAt(bigLVIndex, false);
    }

    /* -------- Methods to handle opcodes --------- */

    private int getOpcodeLoadInstruction(int type) {

        if (type < 0)
            type = -type;

        switch (type) {
            case Type.BOOLEAN:
            case Type.CHAR:
            case Type.BYTE:
            case Type.SHORT:
            case Type.INT:
                return Opcodes.ILOAD;
            case Type.FLOAT:
                return Opcodes.FLOAD;
            case Type.LONG:
                return Opcodes.LLOAD;
            case Type.DOUBLE:
                return Opcodes.DLOAD;
            case Type.ARRAY:
            case Type.OBJECT:
            case Type.METHOD:
                return Opcodes.ALOAD;
            default:
                throw new RuntimeException("Unknown type of element in:"+methodName);
        }

    }

    private int getOpcodeStoreInstruction(int type) {

        if (type < 0)
            type = -type;

        switch (type) {
            case Type.BOOLEAN:
            case Type.CHAR:
            case Type.BYTE:
            case Type.SHORT:
            case Type.INT:
                return Opcodes.ISTORE;
            case Type.FLOAT:
                return Opcodes.FSTORE;
            case Type.LONG:
                return Opcodes.LSTORE;
            case Type.DOUBLE:
                return Opcodes.DSTORE;
            case Type.ARRAY:
            case Type.OBJECT:
            case Type.METHOD:
                return Opcodes.ASTORE;
            default:
                throw new RuntimeException("Unknown type of element.");
        }

    }

    /* ---------------------------------------- */

    MemoryMapperMethodVisitor(final String owner, final int access,
                              final String name, final String desc, final MethodVisitor mv) {
        super(Opcodes.ASM5, mv);

        methodName = String.format(Constants.IDENTIFYING_NAME, owner, name, desc);
        next = mv;

        typesAdapter = new TypesAdapter(owner, access, name, desc, null);
        //localVariablesMap = new ArrayList<>();
        //stackMap = new ArrayList<>();
        bigLVTypes = new ArrayList<>();
        bigLVRefCount = new ArrayList<>();
        freeOneSize = new ArrayList<>();
        freeTwoSize = new ArrayList<>();
        startTry = new Label();
        endTry = new Label();
        startHandleFinally = new Label();
        catchLabels = new HashSet<>();
        uninitializedObjectIdsBigLVIndexes = new HashMap<>();
        bigLVIndexesToLogAfterARCreation = new HashSet<>();


        localVariablesMapBigLVIndexStart = 0;

        uninitializedThisBigLVIndex = -1;
        if ((access & Opcodes.ACC_STATIC) == 0) {
            if( "<init>".equals(name) ) {
                uninitializedThisBigLVIndex = addNewElementToLocalVariablesMap(-Type.OBJECT);
            } else {
                bigLVIndexesToLogAfterARCreation.add(addNewElementToLocalVariablesMap(Type.OBJECT));
            }
        }

        Type[] types = Type.getArgumentTypes(desc);
        for (Type typeT : types) {
            int type = typeT.getSort();
            int tempBigLV = addNewElementToLocalVariablesMap(type);
            if (type == Type.OBJECT) {
                bigLVIndexesToLogAfterARCreation.add(tempBigLV);
            }
        }

    }

    @Override
    public void visitCode() {
        MemoryTracerAgentRuntime.addInstrumentedMethod(methodName);

        while(getBigLVSize() < maxLocals){
            addNewElementToLocalVariablesMap(Type.VOID);
        }

        stackMapBigLVIndexStart = maxLocals;

        for (int i=0; i<maxStack; i++) {
            if (addNewElementToStackMap(Type.VOID) != stackMapBigLVIndexStart+i)
                throw new RuntimeException("Wrong stack slot.");
        }

        tempBigLVIndex1 = getFirstEmptyBigLVIndex(Type.VOID);
        tempBigLVIndex2 = getFirstEmptyBigLVIndex(Type.VOID);

        if (Constants.MEMORY_MAPPER_DEBUG_COMPILE) {
            System.out.println("MaxLocals: " + maxLocals + " maxStack:" + maxStack);
        }

        typesAdapter.visitCode();
        if (Constants.MEMORY_MAPPER_DEBUG_RUNTIME) {
            debugInfo("[BEGIN] " + methodName);
        }

        activationRecordIdBigLVIndex = getFirstEmptyBigLVIndex(activationRecordIdType);
        createActivationRecord();

        if (uninitializedThisBigLVIndex != -1) {
            uninitializedThisIdBigLVIndex = getIdForUninitializedThis(uninitializedThisBigLVIndex);
        } else {
            uninitializedThisIdBigLVIndex = -1;
        }

        for(Integer bigLVIndexToLog : bigLVIndexesToLogAfterARCreation)
            makeLogBigLVIndexAsAddedReference(bigLVIndexToLog);

        super.visitTryCatchBlock(startTry, endTry, startHandleFinally, null);
        super.visitLabel(startTry);
    }

    @Override
    public void visitFieldInsn(int opcode, String owner, String name, String desc) {
        Label startMiniTry = new Label();
        Label endMiniTry = new Label();
        Label catchHandleMiniTry = new Label();
        Label endHandleMiniTry = new Label();
        switch (opcode) {
            case Opcodes.GETFIELD:
                getFromMappedStackAndPushOnOperandStack();
            case Opcodes.GETSTATIC:
                //try{
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                ///instruction
                super.visitFieldInsn(opcode, owner, name, desc);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                if (opcode == Opcodes.GETFIELD) {
                    logRemoveAllStackMap(1);
                } else {
                    logRemoveAllStackMap(0);
                }
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                int returnType = Type.getType(desc).getSort();
                logReturnedValueAsAddedReference(returnType);
                if (opcode == Opcodes.GETFIELD) {
                    popFromMappedStack();
                }
                typesAdapter.visitFieldInsn(opcode, owner, name, desc);
                popFromOperandStackAndPushOnTop0OfMappedStack(returnType, false);
                break;
            case Opcodes.PUTFIELD:
                if (isRefType(getBigLVType(getBigLVIndexOfStackMapTop(0)))) {
                    makeLogBetweenBigLVIndexAsAddedReference(getBigLVIndexOfStackMapTop(1), getBigLVIndexOfStackMapTop(0));
                    getElementFromTopOfMappedStackAndPushOnOperandStack(1);
                    super.visitFieldInsn(Opcodes.GETFIELD, owner, name, desc);
                    popFromOperandStackAndPushOnTemp(Type.getType(desc).getSort(), false);
                    makeLogBetweenBigLVIndexAsRemovedReference(getBigLVIndexOfStackMapTop(1), tempBigLVIndex1);
                }
                get2FromMappedStackAndPushOnOperandStack();
                //try{
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                ///instruction
                super.visitFieldInsn(opcode, owner, name, desc);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(2);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                pop2FromMappedStack();
                typesAdapter.visitFieldInsn(opcode, owner, name, desc);
                break;
            case Opcodes.PUTSTATIC:
                if (isRefType(getBigLVType(getBigLVIndexOfStackMapTop(0)))) {
                    getElementFromTopOfMappedStackAndPushOnOperandStack(0);
                    logAddedRootSetReference();
                    super.visitFieldInsn(Opcodes.GETSTATIC, owner, name, desc);
                    logRemovedRootSetReference();
                }
                getFromMappedStackAndPushOnOperandStack();
                //try{
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                ///instruction
                super.visitFieldInsn(opcode, owner, name, desc);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(1);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                popFromMappedStack();
                typesAdapter.visitFieldInsn(opcode, owner, name, desc);
                break;
        }
    }

    @Override
    public void visitFrame(int type, int nLocal, Object[] local, int nStack, Object[] stack) {
        typesAdapter.visitFrame(type, nLocal, local, nStack, stack);
        super.visitFrame(type, nLocal, local, nStack, stack);
    }

    @Override
    public void visitIincInsn(int var, int increment) {
        typesAdapter.visitIincInsn(var, increment);
        int mapVar = getBigLVIndexOfLocalVariablesMap(var);
        super.visitIincInsn(mapVar, increment);
    }

    @Override
    public void visitInsn(int opcode) {
        Label startMiniTry = new Label();
        Label endMiniTry = new Label();
        Label catchHandleMiniTry = new Label();
        Label endHandleMiniTry = new Label();
        switch (opcode) {
            case Opcodes.NOP:
                typesAdapter.visitInsn(opcode);
                break;
            case Opcodes.ACONST_NULL:
                super.visitInsn(opcode);
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.OBJECT, false);
                break;
            case Opcodes.ICONST_M1:
            case Opcodes.ICONST_0:
            case Opcodes.ICONST_1:
            case Opcodes.ICONST_2:
            case Opcodes.ICONST_3:
            case Opcodes.ICONST_4:
            case Opcodes.ICONST_5:
                super.visitInsn(opcode);
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.INT, false);
                break;
            case Opcodes.LCONST_0:
            case Opcodes.LCONST_1:
                super.visitInsn(opcode);
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.LONG, false);
                break;
            case Opcodes.FCONST_0:
            case Opcodes.FCONST_1:
            case Opcodes.FCONST_2:
                super.visitInsn(opcode);
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.FLOAT, false);
                break;
            case Opcodes.DCONST_0:
            case Opcodes.DCONST_1:
                super.visitInsn(opcode);
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.DOUBLE, false);
                break;
            case Opcodes.IALOAD:
            case Opcodes.BALOAD:
            case Opcodes.CALOAD:
            case Opcodes.SALOAD:
                get2FromMappedStackAndPushOnOperandStack();
                //try{
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                //instruction
                super.visitInsn(opcode);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(2);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                pop2FromMappedStack();
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.INT, false);
                break;
            case Opcodes.LALOAD:
                get2FromMappedStackAndPushOnOperandStack();
                //try{
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                //instruction
                super.visitInsn(opcode);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(2);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                pop2FromMappedStack();
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.LONG, false);
                break;
            case Opcodes.FALOAD:
                get2FromMappedStackAndPushOnOperandStack();
                //try{
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                //instruction
                super.visitInsn(opcode);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(2);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                pop2FromMappedStack();
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.FLOAT, false);
                break;
            case Opcodes.DALOAD:
                get2FromMappedStackAndPushOnOperandStack();
                //try{
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                //instruction
                super.visitInsn(opcode);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(2);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                pop2FromMappedStack();
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.DOUBLE, false);
                break;
            case Opcodes.AALOAD:
                get2FromMappedStackAndPushOnOperandStack();
                //try{
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                //instruction
                super.visitInsn(opcode);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(2);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                logReturnedValueAsAddedReference(Type.OBJECT);
                pop2FromMappedStack();
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.OBJECT, false);
                break;
            case Opcodes.IASTORE:
            case Opcodes.LASTORE:
            case Opcodes.FASTORE:
            case Opcodes.DASTORE:
            case Opcodes.AASTORE:
            case Opcodes.BASTORE:
            case Opcodes.CASTORE:
            case Opcodes.SASTORE:
                getNElementsFromMappedStackAndPushOnOperandStack(3);
                //try{
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                //instruction
                super.visitInsn(opcode);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(3);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                popNElementsFromMappedStack(3);
                typesAdapter.visitInsn(opcode);
                break;
            case Opcodes.POP2:
                if (isTwoSizeType(getBigLVType(getBigLVIndexOfStackMapTop(0)))) {
                    popFromMappedStack();
                } else {
                    pop2FromMappedStack();
                }
                typesAdapter.visitInsn(opcode);
                break;
            case Opcodes.POP:
                popFromMappedStack();
                typesAdapter.visitInsn(opcode);
                break;
            case Opcodes.DUP:
                dupOnMappedStack(1, 1);
                typesAdapter.visitInsn(opcode);
                break;
            case Opcodes.DUP_X1:
                dupOnMappedStack(1, 2);
                typesAdapter.visitInsn(opcode);
                break;
            case Opcodes.DUP_X2:
                dupOnMappedStack(1, 3);
                typesAdapter.visitInsn(opcode);
                break;
            case Opcodes.DUP2:
                dupOnMappedStack(2, 2);
                typesAdapter.visitInsn(opcode);
                break;
            case Opcodes.DUP2_X1:
                dupOnMappedStack(2, 3);
                typesAdapter.visitInsn(opcode);
                break;
            case Opcodes.DUP2_X2:
                dupOnMappedStack(2, 4);
                typesAdapter.visitInsn(opcode);
                break;
            case Opcodes.SWAP:
                swapElementsOnMappedStack();
                typesAdapter.visitInsn(opcode);
                break;
            case Opcodes.IADD:
            case Opcodes.ISUB:
            case Opcodes.IMUL:
            case Opcodes.IAND:
            case Opcodes.IOR:
            case Opcodes.IXOR:
            case Opcodes.ISHL:
            case Opcodes.ISHR:
            case Opcodes.IUSHR:
                get2FromMappedStackAndPushOnOperandStack();
                super.visitInsn(opcode);
                pop2FromMappedStack();
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.INT, false);
                break;
            case Opcodes.IDIV:
            case Opcodes.IREM:
                get2FromMappedStackAndPushOnOperandStack();
                //try {
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                //instruction
                super.visitInsn(opcode);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(2);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                pop2FromMappedStack();
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.INT, false);
                break;
            case Opcodes.LADD:
            case Opcodes.LSUB:
            case Opcodes.LMUL:
            case Opcodes.LAND:
            case Opcodes.LOR:
            case Opcodes.LXOR:
            case Opcodes.LSHL:
            case Opcodes.LSHR:
            case Opcodes.LUSHR:
                get2FromMappedStackAndPushOnOperandStack();
                super.visitInsn(opcode);
                pop2FromMappedStack();
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.LONG, false);
                break;
            case Opcodes.LDIV:
            case Opcodes.LREM:
                get2FromMappedStackAndPushOnOperandStack();
                //try {
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                //instruction
                super.visitInsn(opcode);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(2);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                pop2FromMappedStack();
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.LONG, false);
                break;
            case Opcodes.FADD:
            case Opcodes.FSUB:
            case Opcodes.FMUL:
            case Opcodes.FDIV:
            case Opcodes.FREM:
                get2FromMappedStackAndPushOnOperandStack();
                super.visitInsn(opcode);
                pop2FromMappedStack();
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.FLOAT, false);
                break;
            case Opcodes.DADD:
            case Opcodes.DSUB:
            case Opcodes.DMUL:
            case Opcodes.DDIV:
            case Opcodes.DREM:
                get2FromMappedStackAndPushOnOperandStack();
                super.visitInsn(opcode);
                pop2FromMappedStack();
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.DOUBLE, false);
                break;
            case Opcodes.INEG:
            case Opcodes.F2I:
            case Opcodes.I2B:
            case Opcodes.I2C:
            case Opcodes.I2S:
            case Opcodes.L2I:
            case Opcodes.D2I:
                getFromMappedStackAndPushOnOperandStack();
                super.visitInsn(opcode);
                popFromMappedStack();
                typesAdapter.visitInsn(opcode);
                /*For short, char, byte there is INT push.*/
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.INT, false);
                break;
            case Opcodes.LNEG:
            case Opcodes.I2L:
            case Opcodes.F2L:
            case Opcodes.D2L:
                getFromMappedStackAndPushOnOperandStack();
                super.visitInsn(opcode);
                typesAdapter.visitInsn(opcode);
                popFromMappedStack();
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.LONG, false);
                break;
            case Opcodes.FNEG:
            case Opcodes.I2F:
            case Opcodes.L2F:
            case Opcodes.D2F:
                getFromMappedStackAndPushOnOperandStack();
                super.visitInsn(opcode);
                popFromMappedStack();
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.FLOAT, false);
                break;
            case Opcodes.DNEG:
            case Opcodes.I2D:
            case Opcodes.F2D:
            case Opcodes.L2D:
                getFromMappedStackAndPushOnOperandStack();
                super.visitInsn(opcode);
                popFromMappedStack();
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.DOUBLE, false);
                break;
            case Opcodes.LCMP:
            case Opcodes.DCMPL:
            case Opcodes.DCMPG:
            case Opcodes.FCMPL:
            case Opcodes.FCMPG:
                get2FromMappedStackAndPushOnOperandStack();
                super.visitInsn(opcode);
                popFromMappedStack();
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.INT, false);
                break;
            case Opcodes.ARETURN:
            case Opcodes.IRETURN:
            case Opcodes.LRETURN:
            case Opcodes.FRETURN:
            case Opcodes.DRETURN:
                getFromMappedStackAndPushOnOperandStack();
                if (opcode == Opcodes.ARETURN) {
                    logTopOfOperandStackAsAddedToPreviousActivationRecord();
                }
                popFromMappedStackWithoutLog();
                deleteActivationRecord();
                typesAdapter.visitInsn(opcode);
                if (Constants.MEMORY_MAPPER_DEBUG_RUNTIME) {
                    debugInfo("[END] " + methodName);
                }
                super.visitInsn(opcode);
                break;
            case Opcodes.RETURN:
                deleteActivationRecord();
                typesAdapter.visitInsn(opcode);
                if (Constants.MEMORY_MAPPER_DEBUG_RUNTIME) {
                    debugInfo("[END] " + methodName);
                }
                super.visitInsn(opcode);
                break;
            case Opcodes.ATHROW:
                getFromMappedStackAndPushOnOperandStack();
                popFromMappedStackWithoutLog();
                super.visitInsn(opcode);
                typesAdapter.visitInsn(opcode);
                break;
            case Opcodes.ARRAYLENGTH:
                getFromMappedStackAndPushOnOperandStack();
                //try {
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                //instruction
                super.visitInsn(opcode);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(1);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                popFromMappedStack();
                typesAdapter.visitInsn(opcode);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.INT, false);
                break;
            case Opcodes.MONITORENTER:
            case Opcodes.MONITOREXIT:
                getFromMappedStackAndPushOnOperandStack();
                //try {
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                //instruction
                super.visitInsn(opcode);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(1);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                popFromMappedStack();
                typesAdapter.visitInsn(opcode);
        }
    }

    @Override
    public void visitIntInsn(int opcode, int operand) {
        switch (opcode) {
            case Opcodes.BIPUSH:
            case Opcodes.SIPUSH:
                super.visitIntInsn(opcode, operand);
                typesAdapter.visitIntInsn(opcode, operand);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.INT, false);
                return;
            //case Opcodes.NEWARRAY:
            default:
                Label startMiniTry = new Label();
                Label endMiniTry = new Label();
                Label catchHandleMiniTry = new Label();
                Label endHandleMiniTry = new Label();
                getFromMappedStackAndPushOnOperandStack();
                //try {
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                //instruction
                super.visitIntInsn(opcode, operand);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(1);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                logReturnedValueAsAddedReference(Type.ARRAY);
                popFromMappedStack();
                typesAdapter.visitIntInsn(opcode, operand);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.ARRAY, false);
        }
    }

    @Override
    public void visitInvokeDynamicInsn(String name, String desc, Handle bsm, Object... bsmArgs) {
        Label startMiniTry = new Label();
        Label endMiniTry = new Label();
        Label catchHandleMiniTry = new Label();
        Label endHandleMiniTry = new Label();
        int nargs = Type.getArgumentTypes(desc).length;
        int returnType = Type.getReturnType(desc).getSort();
        getNElementsFromMappedStackAndPushOnOperandStack(nargs);
        //try {
        super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
        super.visitLabel(startMiniTry);
        //instruction
        super.visitInvokeDynamicInsn(name, desc, bsm, bsmArgs);
        //} finally {
        super.visitLabel(endMiniTry);
        super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
        super.visitLabel(catchHandleMiniTry);
        logRemoveAllStackMap(nargs);
        super.visitInsn(Opcodes.ATHROW);
        super.visitLabel(endHandleMiniTry);
        //}
        popNElementsFromMappedStack(nargs);
        typesAdapter.visitInvokeDynamicInsn(name, desc, bsm, bsmArgs);
        if (returnType != Type.VOID) {
            popFromOperandStackAndPushOnTop0OfMappedStack(returnType, false);
        }
    }

    @Override
    public void visitJumpInsn(int opcode, Label label) {
        switch (opcode) {
            case Opcodes.IFEQ:
            case Opcodes.IFNE:
            case Opcodes.IFLT:
            case Opcodes.IFGE:
            case Opcodes.IFGT:
            case Opcodes.IFLE:
            case Opcodes.IFNULL:
            case Opcodes.IFNONNULL:
                popFromMappedStackAndPushOnOperandStack();
                super.visitJumpInsn(opcode, label);
                typesAdapter.visitJumpInsn(opcode, label);
                return;
            case Opcodes.IF_ICMPEQ:
            case Opcodes.IF_ICMPNE:
            case Opcodes.IF_ICMPLT:
            case Opcodes.IF_ICMPGE:
            case Opcodes.IF_ICMPGT:
            case Opcodes.IF_ICMPLE:
            case Opcodes.IF_ACMPEQ:
            case Opcodes.IF_ACMPNE:
                pop2FromMappedStackAndPushOnOperandStack();
                super.visitJumpInsn(opcode, label);
                typesAdapter.visitJumpInsn(opcode, label);
                return;
            case Opcodes.JSR:
                throw new RuntimeException("JSR not supported.");
            //case Opcodes.GOTO:
            default:
                super.visitJumpInsn(opcode, label);
                typesAdapter.visitJumpInsn(opcode, label);
        }
    }

    @Override
    public void visitLdcInsn(Object cst) {
        Label startMiniTry = new Label();
        Label endMiniTry = new Label();
        Label catchHandleMiniTry = new Label();
        Label endHandleMiniTry = new Label();
        //try {
        super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
        super.visitLabel(startMiniTry);
        //instruction
        super.visitLdcInsn(cst);
        //} finally {
        super.visitLabel(endMiniTry);
        super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
        super.visitLabel(catchHandleMiniTry);
        logRemoveAllStackMap(0);
        super.visitInsn(Opcodes.ATHROW);
        super.visitLabel(endHandleMiniTry);
        //}
        typesAdapter.visitLdcInsn(cst);
        if (cst instanceof Integer) {
            popFromOperandStackAndPushOnTop0OfMappedStack(Type.INT, false);
        } else if (cst instanceof Float) {
            popFromOperandStackAndPushOnTop0OfMappedStack(Type.FLOAT, false);
        } else if (cst instanceof Long) {
            popFromOperandStackAndPushOnTop0OfMappedStack(Type.LONG, false);
        } else if (cst instanceof Double) {
            popFromOperandStackAndPushOnTop0OfMappedStack(Type.DOUBLE, false);
        } else if (cst instanceof String) {
            popFromOperandStackAndPushOnTop0OfMappedStack(Type.OBJECT, true);
        } else if (cst instanceof Type) {
            int sort = ((Type) cst).getSort();
            if (sort == Type.OBJECT) {
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.OBJECT, true);
            } else if (sort == Type.ARRAY) {
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.ARRAY, true);
            } else if (sort == Type.METHOD) {
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.METHOD, false);
            } else {
                throw new RuntimeException("LDC instruction - type unsupported, sort:"+sort);
            }
        } else if (cst instanceof Handle) {
            popFromOperandStackAndPushOnTop0OfMappedStack(Type.METHOD, false);
        } else {
            throw new RuntimeException("LDC instruction - type unsupported, cst:"+cst);
        }
    }

    @Override
    public void visitLocalVariable(String name, String desc, String signature, Label start, Label end, int index) {
        typesAdapter.visitLocalVariable(name, desc, signature, start, end, index);
        int bigLVIndex = getBigLVIndexOfLocalVariablesMap(index);
        super.visitLocalVariable(name, desc, signature, start, end, bigLVIndex);
    }

    @Override
    public void visitLookupSwitchInsn(Label dflt, int[] keys, Label[] labels) {
        getFromMappedStackAndPushOnOperandStack();
        super.visitLookupSwitchInsn(dflt, keys, labels);
        popFromMappedStack();
        typesAdapter.visitLookupSwitchInsn(dflt, keys, labels);
    }

    @Override
    public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {

        Label startMiniTry = new Label();
        Label endMiniTry = new Label();
        Label catchHandleMiniTry = new Label();
        Label endHandleMiniTry = new Label();
        int nargs = Type.getArgumentTypes(desc).length;
        int returnType = Type.getReturnType(desc).getSort();
        switch (opcode) {
            case Opcodes.INVOKESTATIC:
                getNElementsFromMappedStackAndPushOnOperandStack(nargs);
                //try {
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                //instruction
                super.visitMethodInsn(opcode, owner, name, desc, itf);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(nargs);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                logReturningFromNotInstrumented(returnType, owner, name, desc);
                popNElementsFromMappedStack(nargs);
                typesAdapter.visitMethodInsn(opcode, owner, name, desc, itf);
                if (returnType != Type.VOID) {
                    popFromOperandStackAndPushOnTop0OfMappedStack(returnType, false);
                }
                break;
            //case Opcodes.INVOKESPECIAL:
            //case Opcodes.INVOKEINTERFACE:
            //case Opcodes.INVOKEVIRTUAL:
            default:
                Set<Integer> addedInstancesBigLVIndexes = new HashSet<>();

                if (opcode == Opcodes.INVOKESPECIAL && name.charAt(0) == '<') {
                    Object t = typesAdapter.stack.get(typesAdapter.getIndexInStackOfTop(nargs));
                    for (int i = 0; i < typesAdapter.locals.size(); ++i) {
                        if (typesAdapter.locals.get(i) == t) {
                            addedInstancesBigLVIndexes.add(getBigLVIndexOfLocalVariablesMap(i));
                        }
                    }
                    for (int i = 0; i < typesAdapter.stack.size(); ++i) {
                        if (typesAdapter.stack.get(i) == t) {
                            addedInstancesBigLVIndexes.add(getBigLVIndexOfStackMapIndex(i));
                        }
                    }

                    changeCurrentUninitializedThis(getBigLVIndexOfStackMapTop(nargs));
                }

                if (Constants.MEMORY_MAPPER_DEBUG_RUNTIME && opcode == Opcodes.INVOKESPECIAL && name.charAt(0) == '<') {
                    debugInfo("[Constructor] "+owner+name+"/"+desc);
                }

                getNElementsFromMappedStackAndPushOnOperandStack(nargs+1);

                //try {
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                //instruction
                super.visitMethodInsn(opcode, owner, name, desc, itf);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(nargs+1);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}

                if (opcode == Opcodes.INVOKESPECIAL && name.charAt(0) == '<') {

                    if (addedInstancesBigLVIndexes.size() > 0) {
                        connectIdWithReference(getBigLVIndexOfStackMapTop(nargs));
                        for (Integer instanceBigLVIndex : addedInstancesBigLVIndexes) {
                            if (getBigLVType(instanceBigLVIndex) >= 0)
                                throw new RuntimeException("Wrong type!");
                            setBigLVType(instanceBigLVIndex, -getBigLVType(instanceBigLVIndex));
                            int idBigLVIndex = uninitializedObjectIdsBigLVIndexes.remove(instanceBigLVIndex);
                            removeElementFromBigLVIndex(idBigLVIndex, true, false);
                        }
                    }
                    resetCurrentUninitializedThis();
                }

                logReturningFromNotInstrumented(returnType, owner, name, desc);
                popNElementsFromMappedStack(nargs+1);
                typesAdapter.visitMethodInsn(opcode, owner, name, desc, itf);
                if (returnType != Type.VOID) {
                    popFromOperandStackAndPushOnTop0OfMappedStack(returnType, false);
                }
        }
    }

    @Override
    public void visitMultiANewArrayInsn(String desc, int dims) {
        Label startMiniTry = new Label();
        Label endMiniTry = new Label();
        Label catchHandleMiniTry = new Label();
        Label endHandleMiniTry = new Label();
        getNElementsFromMappedStackAndPushOnOperandStack(dims);
        //try {
        super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
        super.visitLabel(startMiniTry);
        //instruction
        super.visitMultiANewArrayInsn(desc, dims);
        //} finally {
        super.visitLabel(endMiniTry);
        super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
        super.visitLabel(catchHandleMiniTry);
        logRemoveAllStackMap(dims);
        super.visitInsn(Opcodes.ATHROW);
        super.visitLabel(endHandleMiniTry);
        //}
        logReturnedValueAsAddedReference(Type.ARRAY);
        popNElementsFromMappedStack(dims);
        typesAdapter.visitMultiANewArrayInsn(desc, dims);
        popFromOperandStackAndPushOnTop0OfMappedStack(Type.ARRAY, false);
    }

    @Override
    public void visitTableSwitchInsn(int min, int max, Label dflt, Label... labels) {
        getFromMappedStackAndPushOnOperandStack();
        super.visitTableSwitchInsn(min, max, dflt, labels);
        popFromMappedStack();
        typesAdapter.visitTableSwitchInsn(min, max, dflt, labels);
    }

    @Override
    public void visitTypeInsn(int opcode, String type) {
        Label startMiniTry = new Label();
        Label endMiniTry = new Label();
        Label catchHandleMiniTry = new Label();
        Label endHandleMiniTry = new Label();
        switch (opcode) {
            case Opcodes.ANEWARRAY:
                getFromMappedStackAndPushOnOperandStack();
                //try {
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                //instruction
                super.visitTypeInsn(opcode, type);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(1);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                logReturnedValueAsAddedReference(Type.ARRAY);
                popFromMappedStack();
                typesAdapter.visitTypeInsn(opcode, type);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.ARRAY, false);
                break;
            case Opcodes.CHECKCAST:
                getElementFromTopOfMappedStackAndPushOnOperandStack(0);
                //try {
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                //instruction
                super.visitTypeInsn(opcode, type);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(0);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                super.visitInsn(Opcodes.POP);
                typesAdapter.visitTypeInsn(opcode, type);
                break;
            case Opcodes.NEW:
                //try {
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                //instruction
                super.visitTypeInsn(opcode, type);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(0);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                typesAdapter.visitTypeInsn(opcode, type);
                popFromOperandStackAndPushOnTop0OfMappedStack(-Type.OBJECT, true);
                break;
            //case Opcodes.INSTANCEOF:
            default:
                getFromMappedStackAndPushOnOperandStack();
                //try {
                super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                super.visitLabel(startMiniTry);
                //instruction
                super.visitTypeInsn(opcode, type);
                //} finally {
                super.visitLabel(endMiniTry);
                super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                super.visitLabel(catchHandleMiniTry);
                logRemoveAllStackMap(1);
                super.visitInsn(Opcodes.ATHROW);
                super.visitLabel(endHandleMiniTry);
                //}
                popFromMappedStack();
                typesAdapter.visitTypeInsn(opcode, type);
                popFromOperandStackAndPushOnTop0OfMappedStack(Type.INT, false);
        }
    }

    @Override
    public void visitVarInsn(int opcode, int var) {
        switch (opcode) {
            case Opcodes.ILOAD:
            case Opcodes.LLOAD:
            case Opcodes.FLOAD:
            case Opcodes.DLOAD:
            case Opcodes.ALOAD:
                typesAdapter.visitVarInsn(opcode, var);
                loadFromMappedLocalVariablesAndPushOnMappedStack(var);
                return;
            case Opcodes.ISTORE:
                popFromMappedStackAndStoreIntoLocalVariablesMap(var, Type.INT);
                typesAdapter.visitVarInsn(opcode, var);
                return;
            case Opcodes.LSTORE:
                popFromMappedStackAndStoreIntoLocalVariablesMap(var, Type.LONG);
                typesAdapter.visitVarInsn(opcode, var);
                return;
            case Opcodes.FSTORE:
                popFromMappedStackAndStoreIntoLocalVariablesMap(var, Type.FLOAT);
                typesAdapter.visitVarInsn(opcode, var);
                return;
            case Opcodes.DSTORE:
                popFromMappedStackAndStoreIntoLocalVariablesMap(var, Type.DOUBLE);
                typesAdapter.visitVarInsn(opcode, var);
                return;
            case Opcodes.ASTORE:
                popFromMappedStackAndStoreIntoLocalVariablesMap(var, Type.OBJECT);
                typesAdapter.visitVarInsn(opcode, var);
                return;
            //case Opcodes.RET:
            default:
                typesAdapter.visitVarInsn(opcode, var);
                throw new RuntimeException("RET not supported.");
        }
    }

    @Override
    public void visitTryCatchBlock(Label start, Label end, Label handler, String type) {
        typesAdapter.visitTryCatchBlock(start, end, handler, type);
        super.visitTryCatchBlock(start, end, handler, type);
        catchLabels.add(handler);
    }

    @Override
    public void visitLabel(Label label) {
        typesAdapter.visitLabel(label);
        super.visitLabel(label);
        if (catchLabels.contains(label)) {
            if (Constants.MEMORY_MAPPER_DEBUG_RUNTIME) {
                debugInfo("[INFO] Catch block");
            }
            popFromOperandStackAndPushToPointIndexOnMappedStack(Type.OBJECT, false, 0);
        }
    }

    @Override
    public void visitMaxs(int maxStack, int maxLocals) {
        super.visitLabel(endTry);
        super.visitLabel(startHandleFinally);
        if (Constants.MEMORY_MAPPER_DEBUG_RUNTIME) {
            debugInfo("[INFO] Finalize block for:" + methodName);
        }
        if (Constants.MEMORY_MAPPER_DEBUG_FINALIZE) {
            super.visitInsn(Opcodes.DUP);
            super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime",
                    "handleThrowableRT", getMethodDescriptor(Type.VOID_TYPE, Type.getType(Throwable.class)), false);
        }
        logTopOfOperandStackAsAddedToPreviousActivationRecord();
        deleteActivationRecord();
        super.visitInsn(Opcodes.ATHROW);
        typesAdapter.visitMaxs(maxStack, maxLocals);
    }

}
