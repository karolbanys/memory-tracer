package pl.edu.uj.tcs.memorytracer.agent;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 * @author Karol Banyś.
 */
class SortExceptionHandleClassVisitor extends ClassVisitor {

    SortExceptionHandleClassVisitor(ClassVisitor cv) {
        super(Opcodes.ASM5, cv);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        MethodVisitor mv = super.visitMethod(access, name, desc, signature, exceptions);
        return new SortExceptionHandleMethodVisitor(mv);
    }
}
