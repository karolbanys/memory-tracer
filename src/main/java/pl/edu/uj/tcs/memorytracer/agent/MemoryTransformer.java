package pl.edu.uj.tcs.memorytracer.agent;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.util.CheckClassAdapter;
import pl.edu.uj.tcs.memorytracerruntime.agent.MemoryTracerAgentRuntime;
import pl.edu.uj.tcs.memorytracerruntime.logger.TraceLogger;

import java.io.File;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;


/**
 * @author Karol Banyś.
 */
public class MemoryTransformer implements ClassFileTransformer {

    private String selectedName = null;

    public MemoryTransformer(String name) {
        selectedName = name;
    }

    private boolean isToInstrument(String className, String selectedName){
        if (className == null) {
            return true;
        }

        for (String notInstrumentPackage : Constants.NOT_INSTRUMENT_PACKAGES) {
            if (className.contains(notInstrumentPackage)) {
                return false;
            }
        }

        if (selectedName != null && !className.contains(selectedName)) {
            return false;
        }

        return true;
    }

    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain domain, byte[] bytecode) throws IllegalClassFormatException {
        try {
            MemoryTracerAgentRuntime.increaseAgentRuntime();

            if (!isToInstrument(className, selectedName)) {
                return null;
            }

            if (className != null) {
                for (String saveBeforeClass : Constants.SAVE_BYTECODE_BEFORE_INSTRUMENTATION_CLASSES) {
                    if (className.contains(saveBeforeClass)) {
                        TraceLogger tl2 = new TraceLogger(
                                "TraceLoggerFiles" + File.separator + "InstrumentedClasses" + File.separator + className + "_Before.class");
                        tl2.writeByteArray(bytecode);
                    }
                }
            }

            System.out.println("Thread:["+Thread.currentThread().getName()+"] Instrumenting class: " + className);

            /*------ COMPUTE FRAMES FOR OLDER VERSIONS OF BYTECODE ------*/

            ClassReader readerComputeFrames = new ClassReader(bytecode);
            ComputeClassWriter writerComputeFrames = new ComputeClassWriter(ClassWriter.COMPUTE_FRAMES, loader);
            readerComputeFrames.accept(writerComputeFrames, 0);

            /*------ VISIT BYTECODE ------*/

            ClassReader reader = new ClassReader(writerComputeFrames.toByteArray());
            ComputeClassWriter writer = new ComputeClassWriter(ClassWriter.COMPUTE_FRAMES, loader);

            ClassVisitor checkClassAdapter = new CheckClassAdapter(writer);
            ClassVisitor sortExceptionHandler = new SortExceptionHandleClassVisitor(checkClassAdapter);
            ClassVisitor memoryMapperVisitor = new MemoryMapperClassVisitor(sortExceptionHandler);
            ClassVisitor doubleInstructionsVisitor = new DoubleInstructionsClassVisitor(memoryMapperVisitor);

            reader.accept(doubleInstructionsVisitor, ClassReader.EXPAND_FRAMES);

            System.out.println("Thread:["+Thread.currentThread().getName()+"] End instrumentation: "+ className);
            byte[] bytes = writer.toByteArray();
            if (className != null) {
                for (String saveAfterClass : Constants.SAVE_BYTECODE_AFTER_INSTRUMENTATION_CLASSES) {
                    if (className.contains(saveAfterClass)) {
                        TraceLogger tl = new TraceLogger(
                                "TraceLoggerFiles" + File.separator + "InstrumentedClasses" + File.separator + className + ".class");
                        tl.writeByteArray(bytes);
                    }
                }
            }
            return bytes;
        } catch (Throwable e) {
            if (Constants.MEMORY_MAPPER_DEBUG_COMPILE) {
                System.out.println("[WARNING] Instrumentation error:" + e);
            }
            return null;
        } finally {
            MemoryTracerAgentRuntime.decreaseAgentRuntime();
        }
    }
}
