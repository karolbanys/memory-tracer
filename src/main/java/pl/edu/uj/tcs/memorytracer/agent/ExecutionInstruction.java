package pl.edu.uj.tcs.memorytracer.agent;

/**
 * @author Karol Banyś.
 */
@FunctionalInterface
public interface ExecutionInstruction {
    void executeInstruction();
}