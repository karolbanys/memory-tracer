package pl.edu.uj.tcs.memorytracer.agent;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 * @author Karol Banyś.
 */
class DoubleInstructionsClassVisitor extends ClassVisitor {

    private ClassVisitor memoryMapperClassVisitor;

    DoubleInstructionsClassVisitor(ClassVisitor cv1) {
        super(Opcodes.ASM5, cv1);
        memoryMapperClassVisitor = cv1;
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        MemoryMapperMethodVisitor mv1 = (MemoryMapperMethodVisitor) memoryMapperClassVisitor.visitMethod(access, name, desc, signature, exceptions);
        return new DoubleInstructionsMethodVisitor(mv1, mv1.next);
    }
}
