package pl.edu.uj.tcs.memorytracer.agent;

import pl.edu.uj.tcs.memorytracerruntime.agent.MemoryTracerAgentRuntime;

import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

/**
 * @author Karol Banyś.
 */
public class MemoryTracerAgent {

    public static void premain(String args, Instrumentation instrumentation) {

        String classToTransform = null;
        String logFilePath = null;
        if (args != null) {
            for (String arg : args.split(";")) {
                String[] argNameValue = arg.split(":");
                if (argNameValue.length < 2) {
                    continue;
                }
                if (Constants.CLASS_TO_TRANSFORM.equals(argNameValue[0])) {
                    classToTransform = argNameValue[1];
                }
                if (Constants.LOG_FILE_PATH.equals(argNameValue[0])) {
                    logFilePath = argNameValue[1];
                }
                if (Constants.LOG_UNKNOWN_FROM_ZERO.equals(argNameValue[0])) {
                    if (Constants.LOG_UNKNOWN_FROM_ZERO_FALSE.equals(argNameValue[1])) {
                        MemoryTracerAgentRuntime.setLogUnknownFromZero(false);
                    }
                }
            }
        }
        MemoryTracerAgentRuntime.createTraceLogger(logFilePath);

        // gather previously loaded (modifiable) classes
        ArrayList<Class<?>> classes = new ArrayList<>();
        for (Class<?> clazz : instrumentation.getAllLoadedClasses()) {
            if (instrumentation.isModifiableClass(clazz)) {
                classes.add(clazz);
            }
        }

        instrumentation.addTransformer(new MemoryTransformer(classToTransform), true);

        try {
            instrumentation.retransformClasses(classes.toArray(new Class<?>[classes.size()]));
        } catch (UnmodifiableClassException e) {
            // this should NOT happen
            throw new RuntimeException(e);
        } catch (Throwable e) {
            System.out.println("[ERROR] Instrumentation ERROR:");
            System.out.println(e);
        }


        try {
            MemoryTracerAgentRuntime.increaseAgentRuntime();
            System.out.println("Retransformation finished.");
            for (Class<?> clazz : instrumentation.getAllLoadedClasses()) {
                for (Field field : clazz.getDeclaredFields()) {
                    if (Modifier.isStatic(field.getModifiers()) && !field.getType().isPrimitive()) {
                        try {
                            field.setAccessible(true);
                            Object oField = field.get(null);
                            if (!MemoryTracerAgentRuntime.currentAccessedContainsKey(oField)) {
                                MemoryTracerAgentRuntime.unregisteredSearch(oField, 0);
                            }
                        } catch (Throwable e) {
                        }
                    }
                }
            }
            System.out.println("Searching static fields finished. Found:"+MemoryTracerAgentRuntime.getCurrentId());
        } finally {
            MemoryTracerAgentRuntime.decreaseAgentRuntime();
        }

    }
}
