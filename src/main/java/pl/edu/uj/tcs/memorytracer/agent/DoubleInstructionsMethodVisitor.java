package pl.edu.uj.tcs.memorytracer.agent;

import org.objectweb.asm.*;

import java.util.*;

/**
 * @author Karol Banyś.
 */
class DoubleInstructionsMethodVisitor extends MethodVisitor {

    private MemoryMapperMethodVisitor memoryMapperMethodVisitor;
    private SortExceptionHandleMethodVisitor sortExceptionHandleMethodVisitor;
    private List<ExecutionInstruction> sortExecutionInstructions;
    private List<ExecutionInstruction> mapperExecutionInstructions;
    private Map<Label, Label> labelMap;

    DoubleInstructionsMethodVisitor(MethodVisitor mv1, MethodVisitor mv2) {
        super(Opcodes.ASM5, mv1);
        sortExecutionInstructions = new ArrayList<>();
        mapperExecutionInstructions = new ArrayList<>();
        labelMap = new HashMap<>();
        memoryMapperMethodVisitor = (MemoryMapperMethodVisitor) mv1;
        sortExceptionHandleMethodVisitor = (SortExceptionHandleMethodVisitor) mv2;
    }

    private Label getOrCreateLabel(Label l) {
        if (labelMap.containsKey(l)) {
            return labelMap.get(l);
        }
        Label newL = new Label();
        labelMap.put(l, newL);
        return newL;
    }

    @Override
    public void visitCode() {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitCode();
            }
        });
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitCode();
            }
        });
    }

    @Override
    public void visitParameter(String name, int access) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitParameter(name, access);
            }
        });
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitParameter(name, access);
            }
        });
    }

    @Override
    public void visitAttribute(Attribute attr) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitAttribute(attr);
            }
        });
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitAttribute(attr);
            }
        });
    }

    @Override
    public void visitFrame(int type, int nLocal, Object[] local, int nStack, Object[] stack) {
        Object[] localTemp = Arrays.copyOf(local, local.length);
        Object[] stackTemp = Arrays.copyOf(stack, stack.length);
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitFrame(type, nLocal, localTemp, nStack, stackTemp);
            }
        });
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitFrame(type, nLocal, localTemp, nStack, stackTemp);
            }
        });
    }

    @Override
    public void visitInsn(int opcode) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitInsn(opcode);
            }
        });
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitInsn(opcode);
            }
        });
    }

    @Override
    public void visitIntInsn(int opcode, int operand) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitIntInsn(opcode, operand);
            }
        });
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitIntInsn(opcode, operand);
            }
        });
    }

    @Override
    public void visitVarInsn(int opcode, int var) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitVarInsn(opcode, var);
            }
        });
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitVarInsn(opcode, var);
            }
        });
    }

    @Override
    public void visitTypeInsn(int opcode, String type) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitTypeInsn(opcode, type);
            }
        });
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitTypeInsn(opcode, type);
            }
        });
    }

    @Override
    public void visitFieldInsn(int opcode, String owner, String name, String desc) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitFieldInsn(opcode, owner, name, desc);
            }
        });
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitFieldInsn(opcode, owner, name, desc);
            }
        });
    }

    @Override
    public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitMethodInsn(opcode, owner, name, desc, itf);
            }
        });
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitMethodInsn(opcode, owner, name, desc, itf);
            }
        });
    }

    @Override
    public void visitMethodInsn(int opcode, String owner, String name, String desc) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitMethodInsn(opcode, owner, name, desc);
            }
        });
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitMethodInsn(opcode, owner, name, desc);
            }
        });
    }

    @Override
    public void visitInvokeDynamicInsn(String name, String desc, Handle bsm, Object... bsmArgs) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitInvokeDynamicInsn(name, desc, bsm, bsmArgs);
            }
        });
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitInvokeDynamicInsn(name, desc, bsm, bsmArgs);
            }
        });
    }

    @Override
    public void visitJumpInsn(int opcode, Label label) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitJumpInsn(opcode, label);
            }
        });
        Label mapperLabel = getOrCreateLabel(label);
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitJumpInsn(opcode, mapperLabel);
            }
        });
    }

    @Override
    public void visitLabel(Label label) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitLabel(label);
            }
        });
        Label mapperLabel = getOrCreateLabel(label);
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitLabel(mapperLabel);
            }
        });
    }

    @Override
    public void visitLdcInsn(Object cst) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitLdcInsn(cst);
            }
        });
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitLdcInsn(cst);
            }
        });
    }

    @Override
    public void visitIincInsn(int var, int increment) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitIincInsn(var, increment);
            }
        });
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitIincInsn(var, increment);
            }
        });
    }

    @Override
    public void visitTableSwitchInsn(int min, int max, Label dflt, Label... labels) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitTableSwitchInsn(min, max, dflt, labels);
            }
        });
        Label[] mapperLabels = Arrays.copyOf(labels, labels.length);
        for (int i=0; i<mapperLabels.length; i++) {
            mapperLabels[i] = getOrCreateLabel(mapperLabels[i]);
        }
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitTableSwitchInsn(min, max, dflt, mapperLabels);
            }
        });
    }

    @Override
    public void visitLookupSwitchInsn(Label dflt, int[] keys, Label[] labels) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitLookupSwitchInsn(dflt, keys, labels);
            }
        });
        Label[] mapperLabels = Arrays.copyOf(labels, labels.length);
        for (int i=0; i<mapperLabels.length; i++) {
            mapperLabels[i] = getOrCreateLabel(mapperLabels[i]);
        }
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitLookupSwitchInsn(dflt, keys, mapperLabels);
            }
        });
    }

    @Override
    public void visitMultiANewArrayInsn(String desc, int dims) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitMultiANewArrayInsn(desc, dims);
            }
        });
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitMultiANewArrayInsn(desc, dims);
            }
        });
    }

    @Override
    public void visitTryCatchBlock(Label start, Label end, Label handler, String type) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitTryCatchBlock(start, end, handler, type);
            }
        });
        Label mapperStartLabel = getOrCreateLabel(start);
        Label mapperEndLabel = getOrCreateLabel(end);
        Label mapperHandlerLabel = getOrCreateLabel(handler);
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitTryCatchBlock(mapperStartLabel, mapperEndLabel, mapperHandlerLabel, type);
            }
        });
    }

    @Override
    public void visitLocalVariable(String name, String desc, String signature, Label start, Label end, int index) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitLocalVariable(name, desc, signature, start, end, index);
            }
        });
        Label mapperStartLabel = getOrCreateLabel(start);
        Label mapperEndLabel = getOrCreateLabel(end);
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitLocalVariable(name, desc, signature, mapperStartLabel, mapperEndLabel, index);
            }
        });
    }

    @Override
    public void visitLineNumber(int line, Label start) {
        sortExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                sortExceptionHandleMethodVisitor.visitLineNumber(line, start);
            }
        });
        Label mapperLabel = getOrCreateLabel(start);
        mapperExecutionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                memoryMapperMethodVisitor.visitLineNumber(line, mapperLabel);
            }
        });
    }

    @Override
    public void visitMaxs(int maxStack, int maxLocals) {
        sortExceptionHandleMethodVisitor.visitCode();
        Label elseLabel = new Label();
        sortExceptionHandleMethodVisitor.visitMethodInsn(Opcodes.INVOKESTATIC,
                "pl/edu/uj/tcs/memorytracerruntime/agent/MemoryTracerAgentRuntime", "isLoggingEnabledRT",
                Type.getMethodDescriptor(Type.BOOLEAN_TYPE), false);
        sortExceptionHandleMethodVisitor.visitJumpInsn(Opcodes.IFNE, elseLabel);
        for (ExecutionInstruction executionInstruction : sortExecutionInstructions) {
            executionInstruction.executeInstruction();
        }
        Label endLabel = new Label();
        sortExceptionHandleMethodVisitor.visitJumpInsn(Opcodes.GOTO, endLabel);
        sortExceptionHandleMethodVisitor.visitLabel(elseLabel);
        memoryMapperMethodVisitor.maxLocals = maxLocals;
        memoryMapperMethodVisitor.maxStack =  maxStack;
        for (ExecutionInstruction executionInstruction : mapperExecutionInstructions) {
            executionInstruction.executeInstruction();
        }
        memoryMapperMethodVisitor.visitMaxs(maxStack, maxLocals);
        sortExceptionHandleMethodVisitor.visitLabel(endLabel);
        sortExceptionHandleMethodVisitor.visitMaxs(memoryMapperMethodVisitor.getBigLVSize(), memoryMapperMethodVisitor.getBigLVSize());
    }

    @Override
    public void visitEnd() {
        memoryMapperMethodVisitor.visitEnd();
    }
}
