package pl.edu.uj.tcs.memorytracer.agent;

import org.objectweb.asm.Label;

/**
 * @author Karol Banyś.
 */
class ExceptionHandler implements Comparable<ExceptionHandler>{
    Label start;
    Label end;
    Label handle;
    String type;

    int startNumber;
    int endNumber;

    ExceptionHandler(Label start, Label end, Label handle, String type) {
        this.start = start;
        this.end = end;
        this.handle = handle;
        this.type = type;
    }

    @Override
    public int compareTo(ExceptionHandler o) {
        return Integer.compare(endNumber, o.endNumber);
    }
}
