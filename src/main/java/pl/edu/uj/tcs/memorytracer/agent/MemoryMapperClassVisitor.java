package pl.edu.uj.tcs.memorytracer.agent;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 * @author Karol Banyś.
 */
class MemoryMapperClassVisitor extends ClassVisitor{

    private String className;

    MemoryMapperClassVisitor(ClassVisitor cv) { super(Opcodes.ASM5, cv); }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces){
        super.visit(version, access, name, signature, superName, interfaces);
        className = name;
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        MethodVisitor mv = super.visitMethod(access, name, desc, signature, exceptions);
        if (Constants.MEMORY_MAPPER_DEBUG_COMPILE) {
            System.out.println("Class:" + className + " method:" + name + " " + desc + " " + signature);
        }
        return new MemoryMapperMethodVisitor(className, access, name, desc, mv);
    }
}
