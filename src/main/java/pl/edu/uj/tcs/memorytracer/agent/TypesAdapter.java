package pl.edu.uj.tcs.memorytracer.agent;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.commons.AnalyzerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Karol Banyś.
 */
class TypesAdapter extends AnalyzerAdapter {

    TypesAdapter(String owner, int access, String name, String desc,
                           MethodVisitor mv) {
        super(Opcodes.ASM5, owner, access, name, desc, mv);
    }

    public void print(String header){
        System.out.println(header);
        System.out.println("Locals:");
        for(Object l : locals) {
            System.out.print(l+", ");
        }
        System.out.println();
        System.out.println("Stack:");
        for(Object s : stack) {
            System.out.print(s+", ");
        }
        System.out.println();
    }

    /**
     * Primitive types long and double are represented by 2 slots in Stack.
     * Method truncates long and double to 1 slot.
     * @return List of objects.
     */
    List<Object> getTruncatedStack(){
        List<Object> stackCopy = new ArrayList<>();
        for(Object elem : stack) {
            if (!Opcodes.TOP.equals(elem)) {
                stackCopy.add(elem);
            }
        }
        return stackCopy;
    }

    /**
     * Method iterate from top of stack and return index of n-th element.
     * @param n number of element from top (0, 1, ...)
     * @return index in stack of n-th value from top
     */
    int getIndexInStackOfTop(int n) {
        if (stack == null)
            return 0;
        int i = stack.size()-1;
        for (; i>0; i--) {
            if (Opcodes.TOP.equals(stack.get(i))) {
                i--;
            }
            if (n==0)
                return i;
            n--;
        }
        return i;
    }

    /**
     * Return next index of element in stack.
     * Avoid slots with Opcodes.TOP.
     * @param currentIndex current position in stack
     * @return index of next position in stack
     */
    int nextIndexInStack(int currentIndex) {
        int nextIndex = currentIndex+1;
        if (nextIndex < stack.size() && Opcodes.TOP.equals(stack.get(nextIndex))) {
            nextIndex++;
        }
        return nextIndex;
    }

    /**
     * Return prev index of element in stack.
     * Avoid slots with Opcodes.TOP.
     * @param currentIndex current position in stack
     * @return index of prev position in stack
     */
    int prevIndexInStack(int currentIndex) {
        int prevIndex = currentIndex-1;
        if (0 <= prevIndex && Opcodes.TOP.equals(stack.get(prevIndex))) {
            prevIndex--;
        }
        return prevIndex;
    }

    boolean isObject(Object elem) {
        return elem instanceof String
                || elem instanceof Label
                || Opcodes.UNINITIALIZED_THIS.equals(elem);
    }


}
