package pl.edu.uj.tcs.archive.agent;

/**
 * @author Karol Banyś.
 */
public class MemoryMapperMethodVisitorArchive {

    /**
     * Variables measuring operand stack.
     */
    private int operandStackMax;
    private int operandStackSize;

    /**
     * Updating size of operand stack after added value.
     * @param type - type of added value
     */
    /*private void updateAddOperandStackSize(int type) {
        operandStackSize += typeSize(type);
        operandStackMax = operandStackSize > operandStackMax ? operandStackSize : operandStackMax;
    }*/

    /**
     * Updating size of operand stack after removed value.
     * @param type - type of removed value
     */
    /*private void updateRemoveOperandStackSize(int type) {
        operandStackSize -= typeSize(type);
    }*/

    /**
     * Get map index of local variables table in position n.
     * @param n - number of element
     * @param type - type of element
     * @return - map index were insert value
     */
    /*private int getNewLocalVariablesBigLVIndexFor(int n, int type) {
        if (n >= localVariablesMap.size()) {
            int bigLVIndex = addNewElementToLocalVariablesMap(type);
            if (n >= localVariablesMap.size()){
                throw new RuntimeException("Wrong size of local variables."+
                        "Is:"+localVariablesMap.size()+" should be:"+n);
            }
            return bigLVIndex;
        }

        int bigLVIndex = localVariablesMap.get(n);

        int newBigLVIndex = handleRemappingBigLV(bigLVIndex, type);

        if (newBigLVIndex != bigLVIndex) {
            updateLocalVariablesMap(n, newBigLVIndex);
        }

        return newBigLVIndex;
    }*/


        /*private void revertUninitializedThis() {
        changeCurrentUninitializedThis(previousUninitializedThisId);
        super.visitInsn(Opcodes.POP2);
    }

    private void changeCurrentUninitializedThisWithSaveOld(int bigLVIndex) {
        int idBigLVIndex = uninitializedObjectIdsBigLVIndexes.get(bigLVIndex);
        changeCurrentUninitializedThis(idBigLVIndex);
        popFromOperandStackAndStoreAt(previousUninitializedThisId, false);
    }*/


    /**
     * Update local variables map at index n and put there bigLVIndex.
     * @param n - index where local variables map should be updated
     * @param bigLVIndex - index in bigLV which should be insert into local variables map
     */
    /*private void updateLocalVariablesMap(int n, int bigLVIndex) {
        int type = getBigLVType(bigLVIndex);
        if (n < localVariablesMap.size()) {
            localVariablesMap.set(n, bigLVIndex);
            if (isTwoSizeType(type)) {
                localVariablesMap.set(n + 1, bigLVIndex);
            }
        } else if (localVariablesMap.size() == n) {
            localVariablesMap.add(bigLVIndex);
            if (isTwoSizeType(type)) {
                localVariablesMap.add(bigLVIndex);
            }
        } else {
            throw new RuntimeException("Too long place in LV.");
        }
    }*/

    /**
     * Free map index from index n in local variables map.
     * @param n - index to remove
     * @return bigLVIndex of removed index
     */
    /*private int freeLocalVariablesMapAt(int n){
        int bigLVIndex = localVariablesMap.get(n);
        freeBigLVIndex(bigLVIndex);
        return bigLVIndex;
    }*/

    /**
     * Store top of operand stack in n-th element in local variables table.
     * @param n - index of element in local variables map to store
     * @param type - type of element to store
     */
    /*private void popFromOperandStackAndStoreIntoLocalVariablesMap(int n, int type) {
        int bigLVIndex = getNewLocalVariablesBigLVIndexFor(n, type);
        popFromOperandStackAndStoreAt(bigLVIndex);
        updateLocalVariablesMap(n, bigLVIndex);
    }*/

    /**
     * Duplicate n-th value on map stack.
     *
     * @param n - top index of value to duplicate top0 (0, 1, ...)
     * @return bigLVIndex in bigLV of duplicate value
     */
    /*private int dupOnMappedStackWithRefCounting(int n) {
        int bigLVIndex = getBigLVIndexOfStackMapTop(n);
        increaseBigLVRefCount(bigLVIndex);
        return bigLVIndex;
    }*/

    /**
     * Duplicate top element of map stack and put on i-th place from top0.
     *
     * @param i - size of elements from stack to put from top0, 0 is after top0.
     */
    /*private void dupOnMappedStackAndPutOnPlaceWithRefCounting(int i) {
        int putPlace = 0;
        i += 1;
        while (i > 0) {
            i -= typeSize(getBigLVType(getBigLVIndexOfStackMapTop(putPlace)));
            putPlace++;
        }
        int putPlaceTopCounted = putPlace-1;
        int clonedBigLVIndex = dupOnMappedStack(0);
        stackMap.add(getStackMapIndexOfStackMapTop(putPlaceTopCounted), clonedBigLVIndex);
    }*/

    /**
     * Duplicate 2 top element of map stack and put on i-th place from top0.
     *
     * @param  - i - size of elements from stack to put from top0, 0 is after top1.
     */
    /*private void dup2OnMappedStackAndPutOnPlaceWithRefCounting(int i) {
        int putPlace = 0;
        i += 2;
        while (i > 0) {
            i -= typeSize(getBigLVType(getBigLVIndexOfStackMapTop(putPlace)));
            putPlace++;
        }
        int cloneTop0BigLVIndex = dupOnMappedStack(0);
        int putPlaceTopCounted = putPlace-1;
        switch (getBigLVType(cloneTop0BigLVIndex)) {
            case Type.LONG:
            case Type.DOUBLE:
                stackMap.add(getStackMapIndexOfStackMapTop(putPlaceTopCounted), cloneTop0BigLVIndex);
                break;
//            case Type.INT:
//            case Type.BYTE:
//            case Type.FLOAT:
//            case Type.CHAR:
//            case Type.SHORT:
//            case Type.OBJECT:
//            case Type.ARRAY
//            case Type.METHOD:
            default:
                int cloneTop1BigLVIndex = dupOnMappedStack(1);
                List<Integer> dupElements = new ArrayList<>();
                dupElements.add(cloneTop1BigLVIndex);
                dupElements.add(cloneTop0BigLVIndex);
                stackMap.addAll(getStackMapIndexOfStackMapTop(putPlaceTopCounted), dupElements);
        }
    }*/

    /*getElementFromTopOfMappedStackAndPushOnOperandStack(0);
                super.visitTypeInsn(Opcodes.INSTANCEOF, type);
                Label lNoError = new Label();
                Label lEnd = new Label();
                super.visitJumpInsn(Opcodes.IFNE, lNoError);
                    getElementFromTopOfMappedStackAndPushOnOperandStack(0);
                    super.visitJumpInsn(Opcodes.IFNULL, lNoError);
                    getFromMappedStackAndPushOnOperandStack();
                    //try {
                    super.visitTryCatchBlock(startMiniTry, endMiniTry, catchHandleMiniTry, "java/lang/Exception");
                    super.visitLabel(startMiniTry);
                    //instruction
                    super.visitTypeInsn(opcode, type);
                    //} finally {
                    super.visitLabel(endMiniTry);
                    super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry);
                    super.visitLabel(catchHandleMiniTry);
                    logRemoveAllStackMap(1);
                    super.visitInsn(Opcodes.ATHROW);
                    super.visitLabel(endHandleMiniTry);
                    //}
                    logReturnedValueAsAddedReference(Type.OBJECT);
                    popFromMappedStack();
                    typesAdapter.visitTypeInsn(opcode, type);
                    popFromOperandStackAndPushOnTop0OfMappedStack(Type.OBJECT, false);
                    super.visitJumpInsn(Opcodes.GOTO, lEnd);
                super.visitLabel(lNoError);
                    Label startMiniTry2 = new Label();
                    Label endMiniTry2 = new Label();
                    Label catchHandleMiniTry2 = new Label();
                    Label endHandleMiniTry2 = new Label();
                    getFromMappedStackAndPushOnOperandStack();
                    //try {
                    super.visitTryCatchBlock(startMiniTry2, endMiniTry2, catchHandleMiniTry2, "java/lang/Exception");
                    super.visitLabel(startMiniTry2);
                    //instruction
                    super.visitTypeInsn(opcode, type);
                    //} finally {
                    super.visitLabel(endMiniTry2);
                    super.visitJumpInsn(Opcodes.GOTO, endHandleMiniTry2);
                    super.visitLabel(catchHandleMiniTry2);
                    logRemoveAllStackMap(1);
                    super.visitInsn(Opcodes.ATHROW);
                    super.visitLabel(endHandleMiniTry2);
                    //}
                    popFromMappedStack();
                    typesAdapter.visitTypeInsn(opcode, type);
                super.visitLabel(lEnd);*/
}
