package pl.edu.uj.tcs.archive.agent;

import org.objectweb.asm.*;
import org.objectweb.asm.commons.AnalyzerAdapter;

/**
 * @author Karol Banyś.
 */
public class AddEdgesMethodVisitor extends AnalyzerAdapter {

    private AddVariablesMethodVisitor variablesMethodVisitor;
    private int id;

    public AddEdgesMethodVisitor(int api, String owner, int access, String name, String desc,
                          MethodVisitor methodVisitor) {
        super(api, owner, access, name, desc, methodVisitor);
        variablesMethodVisitor = (AddVariablesMethodVisitor) methodVisitor;
    }

    private void putActiveRecordsIdOnStack() {
        super.visitVarInsn(Opcodes.LLOAD, id);
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracer/agent/MemoryTracerAgent", "returnObjectFromId",
                Type.getMethodDescriptor(Type.getType(Object.class), Type.LONG_TYPE), false);
    }

    //MemoryTracerAgent.addedReference(top1, top0)
    private void logAddedReference() {
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracer/agent/MemoryTracerAgent", "addedReference",
                Type.getMethodDescriptor(Type.VOID_TYPE, Type.getType(Object.class), Type.getType(Object.class)),
                false);
    }

    //MemoryTracerAgent.removedReference(top1, top0)
    private void logRemovedReference() {
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracer/agent/MemoryTracerAgent", "removedReference",
                Type.getMethodDescriptor(Type.VOID_TYPE, Type.getType(Object.class), Type.getType(Object.class)),
                false);
    }

    private void dupTopOfStack() {
        super.visitInsn(Opcodes.DUP);
    }

    private void loadObjectFromLV(int i) {
        super.visitVarInsn(Opcodes.ALOAD, i);
    }

    private void makeLogTopOfStackAsAddedReference() {
        //to
        dupTopOfStack();
        //from
        putActiveRecordsIdOnStack();
        //MemoryTracerAgent.removedReference(to, from)
        logAddedReference();
    }

    private void makeLogTopOfStackAsRemovedReference() {
        //to
        dupTopOfStack();
        //from
        putActiveRecordsIdOnStack();
        //MemoryTracerAgent.addedReference(to, from)
        logRemovedReference();
    }

    @Override
    public void visitCode() {
        super.visitCode();
        id = variablesMethodVisitor.getId();

        //write edge from current thread to method
        putActiveRecordsIdOnStack();
        super.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Thread", "currentThread", Type.getMethodDescriptor(Type.getObjectType("java/lang/Thread")) ,false);
        logAddedReference();

        //log add all object references to LV
        for (int i = 1; i < locals.size(); i++) {
            Object local = locals.get(i);
            if (local instanceof String) {
                //to
                loadObjectFromLV(i);
                //from
                putActiveRecordsIdOnStack();
                //MemoryTracerAgent.addedReference(to, from)
                logAddedReference();
            }
        }
    }

    @Override
    public void visitInsn(int opcode) {

        //log remove all object references from LV
        if ((opcode >= Opcodes.IRETURN && opcode <= Opcodes.RETURN) || opcode == Opcodes.ATHROW) {
            for (int i = 1; i < locals.size(); i++) {
                Object local = locals.get(i);
                if (local instanceof String) {
                    //to
                    loadObjectFromLV(i);
                    //from
                    putActiveRecordsIdOnStack();
                    //MemoryTracerAgent.removedReference(to, from)
                    logRemovedReference();
                }
            }

            //remove current thread
            //write edge from current thread to method
            super.visitMethodInsn(Opcodes.INVOKESTATIC, "java/lang/Thread", "currentThread", Type.getMethodDescriptor(Type.getObjectType("java/lang/Thread")) ,false);
            putActiveRecordsIdOnStack();
            logRemovedReference();
        }

        switch (opcode) {
            case Opcodes.AALOAD:
                //log remove top1 (array ref) from stack
                super.visitInsn(Opcodes.DUP2);
                super.visitInsn(Opcodes.POP);
                putActiveRecordsIdOnStack();
                logRemovedReference();
                super.visitInsn(Opcodes.AALOAD);
                //log add object ref to stack
                makeLogTopOfStackAsAddedReference();
                return;
            case Opcodes.AASTORE:
                break;
            case Opcodes.ARETURN:
                //log remove object ref (which is returned)
                makeLogTopOfStackAsRemovedReference();
                break;
            case Opcodes.ARRAYLENGTH:
                //log remove array ref from stack
                makeLogTopOfStackAsRemovedReference();
                break;
            case Opcodes.ATHROW:
                break;
            case Opcodes.BALOAD:
            case Opcodes.CALOAD:
            case Opcodes.DALOAD:
            case Opcodes.FALOAD:
            case Opcodes.IALOAD:
            case Opcodes.LALOAD:
            case Opcodes.SALOAD:
                //log remove top1 (array ref) from stack
                super.visitInsn(Opcodes.DUP2);
                super.visitInsn(Opcodes.POP);
                putActiveRecordsIdOnStack();
                logRemovedReference();
                break;
            case Opcodes.BASTORE:
            case Opcodes.CASTORE:
            case Opcodes.DASTORE:
            case Opcodes.FASTORE:
            case Opcodes.IASTORE:
            case Opcodes.LASTORE:
            case Opcodes.SASTORE:
                break;
            case Opcodes.DUP:
            case Opcodes.DUP_X1:
            case Opcodes.DUP_X2:
                //log add top to stack (if object type)
                if (stack.get(0) instanceof String) {
                    makeLogTopOfStackAsAddedReference();
                }
                break;
            case Opcodes.DUP2:
            case Opcodes.DUP2_X1:
            case Opcodes.DUP2_X2:
                super.visitInsn(Opcodes.DUP2);

                //log add top0 to stack
                if (stack.get(0) instanceof String) {
                    //from
                    putActiveRecordsIdOnStack();
                    //MemoryTracerAgent.addedReference(to, from)
                    logAddedReference();
                }
                //if not object type just remove from stack
                else {
                    super.visitInsn(Opcodes.POP);
                }

                //log add top1 to stack (if object type)
                //on top of stack there is second duplicated value
                if (stack.get(0) instanceof String) {
                    //from
                    putActiveRecordsIdOnStack();
                    //MemoryTracerAgent.addedReference(to, from)
                    logAddedReference();
                }
                //if not object type just remove from stack
                else {
                    super.visitInsn(Opcodes.POP);
                }

                break;
            case Opcodes.MONITORENTER:
            case Opcodes.MONITOREXIT:
                //log remove reference from stack
                makeLogTopOfStackAsRemovedReference();
                break;
            case Opcodes.POP:
                //log remove top from stack (if object type)
                if (stack.get(0) instanceof String) {
                    makeLogTopOfStackAsRemovedReference();
                }
                break;
            case Opcodes.POP2:
                super.visitInsn(Opcodes.DUP2);

                //log remove top0 from stack (if object type)
                if (stack.get(0) instanceof String) {
                    //from
                    putActiveRecordsIdOnStack();
                    //MemoryTracerAgent.removedReference(to, from)
                    logRemovedReference();
                }
                //if not object type just remove from stack
                else {
                    super.visitInsn(Opcodes.POP);
                }

                //log remove top1 from stack (if object type)
                //on top of stack there is second duplicated value
                if (stack.get(0) instanceof String) {
                    //from
                    putActiveRecordsIdOnStack();
                    //MemoryTracerAgent.removedReference(to, from)
                    logRemovedReference();
                }
                //if not object type just remove from stack
                else {
                    super.visitInsn(Opcodes.POP);
                }

                break;
        }


        super.visitInsn(opcode);
    }

    @Override
    public void visitIntInsn(int opcode, int operand) {
        switch (opcode) {
            case Opcodes.NEWARRAY:
                super.visitIntInsn(opcode, operand);
                //log add array ref to stack
                makeLogTopOfStackAsAddedReference();
                return;
        }
        super.visitIntInsn(opcode, operand);
    }

    @Override
    public void visitVarInsn(int opcode, int var) {

        switch (opcode) {
            case Opcodes.ASTORE:

                //log remove reference from Stack
                makeLogTopOfStackAsRemovedReference();

                //log add reference to LV
                makeLogTopOfStackAsAddedReference();

                //log remove reference from LV if add to existing record in LV
                if (var < locals.size()) {
                    //to
                    loadObjectFromLV(var);
                    //from
                    putActiveRecordsIdOnStack();
                    //MemoryTracerAgent.removedReference(to, from)
                    logRemovedReference();
                }

                break;
            case Opcodes.ALOAD:
                super.visitVarInsn(opcode, var);
                //log add reference to stack
                makeLogTopOfStackAsAddedReference();
                return;
        }

        super.visitVarInsn(opcode, var);
    }

    @Override
    public void visitTypeInsn(int opcode, String type) {

        switch (opcode) {
            case Opcodes.ANEWARRAY:
                super.visitTypeInsn(opcode, type);
                //log add array ref to stack
                makeLogTopOfStackAsAddedReference();
                return;
            case Opcodes.CHECKCAST:
                //if object ref is null then the operand stack is unchanged
                break;
            case Opcodes.INSTANCEOF:
                //log remove reference from stack
                makeLogTopOfStackAsRemovedReference();
                break;
            case Opcodes.NEW:
                super.visitTypeInsn(opcode, type);
                //log add new object ref to stack
                makeLogTopOfStackAsAddedReference();
                return;
        }

        super.visitTypeInsn(opcode, type);
    }

    @Override
    public void visitFieldInsn(int opcode, String owner, String name, String desc) {

        switch (opcode) {
            case Opcodes.GETFIELD:
                //log remove object ref from stack
                makeLogTopOfStackAsRemovedReference();
            case Opcodes.GETSTATIC:
                super.visitFieldInsn(opcode, owner, name, desc);
                //log add field value to stack (if object type)
                if (stack.get(0) instanceof String){
                    makeLogTopOfStackAsAddedReference();
                }
                return;
            case Opcodes.PUTFIELD:
                super.visitInsn(Opcodes.DUP2);

                //log remove field value from stack (if object type)
                if (stack.get(0) instanceof String) {
                    putActiveRecordsIdOnStack();
                    logRemovedReference();
                }
                //remove if not object type
                else {
                    super.visitInsn(Opcodes.POP);
                }

                //log remove object ref from stack
                putActiveRecordsIdOnStack();
                logRemovedReference();
                break;
            case Opcodes.PUTSTATIC:
                //log remove field value from stack (if object type)
                if (stack.get(0) instanceof String) {
                    makeLogTopOfStackAsAddedReference();
                }
                break;
        }

        super.visitFieldInsn(opcode, owner, name, desc);
    }

    @Override
    public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {

        switch (opcode) {
            case Opcodes.INVOKEINTERFACE:
            case Opcodes.INVOKESPECIAL:
            case Opcodes.INVOKEVIRTUAL:
                break;
            case Opcodes.INVOKESTATIC:
                break;
        }

        System.out.println(String.format("Owner:%s, name:%s", name, desc));
        super.visitMethodInsn(opcode, owner, name, desc, itf);
    }

    @Override
    public void visitInvokeDynamicInsn(String name, String desc, Handle bsm, Object... bsmArgs) {

        super.visitInvokeDynamicInsn(name, desc, bsm, bsmArgs);
    }

    @Override
    public void visitJumpInsn(int opcode, Label label) {

        switch (opcode) {
            case Opcodes.IF_ACMPEQ:
            case Opcodes.IF_ACMPNE:
                super.visitInsn(Opcodes.DUP2);

                //log remove top0 ref from stack
                putActiveRecordsIdOnStack();
                logRemovedReference();

                //log remove top1 ref from stack
                putActiveRecordsIdOnStack();
                logRemovedReference();
                break;
            case Opcodes.IFNONNULL:
            case Opcodes.IFNULL:
                //log remove reference from stack
                makeLogTopOfStackAsRemovedReference();
                break;
        }

        super.visitJumpInsn(opcode, label);
    }

    @Override
    public void visitLdcInsn(Object cst) {

        super.visitLdcInsn(cst);

        //log add loaded constant (if object ref or array ref)
        if (cst instanceof Type) {
            int sort = ((Type) cst).getSort();
            if (sort == Type.OBJECT || sort == Type.ARRAY) {
                makeLogTopOfStackAsAddedReference();
            }
        }

    }

    @Override
    public void visitMultiANewArrayInsn(String desc, int dims) {

        super.visitMultiANewArrayInsn(desc, dims);

        //log add reference to stack
        makeLogTopOfStackAsAddedReference();
    }


}
