package pl.edu.uj.tcs.archive.agent;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;

/**
 * @author Karol Banyś.
 */
class PreProcessingInformationClassVisitor extends ClassVisitor {

    private String className;

    PreProcessingInformationClassVisitor(int api, ClassVisitor cv) {
        super(api, cv);
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        super.visit(version, access, name, signature, superName, interfaces);
        className = name;
    }

    @Override
    public MethodVisitor visitMethod(int access, String methodName, String desc, String signature, String[] exceptions) {
        MethodVisitor mv = cv.visitMethod(access, methodName, desc, signature, exceptions);
        return new PreProcessingInformationMethodVisitor(api, className, methodName, desc, mv);
    }
}
