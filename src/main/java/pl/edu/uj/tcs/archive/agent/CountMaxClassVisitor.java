package pl.edu.uj.tcs.archive.agent;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 * Created by Karol on 2016-09-19.
 */
public class CountMaxClassVisitor extends ClassVisitor{

    public CountMaxClassVisitor(ClassVisitor cv) {
        super(Opcodes.ASM5, cv);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        MethodVisitor mv = super.visitMethod(access, name, desc, signature, exceptions);
        return new CountMaxMethodVisitor(mv);
    }
}
