package pl.edu.uj.tcs.archive.agent;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import pl.edu.uj.tcs.memorytracerruntime.agent.MemoryTracerAgentRuntime;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

/**
 * @author Karol Banyś.
 */
public class PreProcessingInformationTransformer implements ClassFileTransformer {

    private String selectedName = null;

    public PreProcessingInformationTransformer(String name) {
        selectedName = name;
    }

    @Override
    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
                            ProtectionDomain protectionDomain, byte[] classfileBuffer) throws
            IllegalClassFormatException {
        try {
            MemoryTracerAgentRuntime.increaseAgentRuntime();
            /*for (String notInstrumentThread : Constants.NOT_INSTRUMENT_THREADS) {
                if (Thread.currentThread().getName().equals(notInstrumentThread)) {
                    return null;
                }
            }*/

            /*for (String notInstrumentPackage : Constants.NOT_INSTRUMENT_PACKAGES) {
                if (className.contains(notInstrumentPackage)) {
                    return null;
                }
            }*/

            if (selectedName != null && !className.contains(selectedName))
                return null;

            System.out.println("Thread:["+Thread.currentThread().getName()+"] Preprocessing class: " + className);

            ClassReader reader = new ClassReader(classfileBuffer);
            ClassWriter writer = new ClassWriter(reader, ClassWriter.COMPUTE_FRAMES);

            ClassVisitor preProcessingInformationClassVisitor = new PreProcessingInformationClassVisitor(Opcodes.ASM5,
                    writer);

            reader.accept(preProcessingInformationClassVisitor, ClassReader.EXPAND_FRAMES);

            System.out.println("Thread:["+Thread.currentThread().getName()+"] End preprocessing: " + className);

            return writer.toByteArray();
        } catch (Throwable e) {
            System.out.println("Instrumentation error:" + e);
            e.printStackTrace();
            return null;
        } finally {
            MemoryTracerAgentRuntime.decreaseAgentRuntime();
        }
    }
}
