package pl.edu.uj.tcs.archive.agent;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 * Created by Karol Banyś.
 */
public class AddEdgesClassVisitor extends ClassVisitor {

    private String className;

    public AddEdgesClassVisitor(int api) {
        super(api);
    }

    public AddEdgesClassVisitor(int api, ClassVisitor cv){
        super(api, cv);
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces){
        super.visit(version, access, name, signature, superName, interfaces);
        className = name;
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        MethodVisitor mv = cv.visitMethod(access, name, desc, signature, exceptions);
        return new AddEdgesMethodVisitor(Opcodes.ASM5, className, access, name, desc, mv);
    }
}
