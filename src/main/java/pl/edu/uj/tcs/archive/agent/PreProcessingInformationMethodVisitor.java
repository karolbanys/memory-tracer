package pl.edu.uj.tcs.archive.agent;

import org.objectweb.asm.MethodVisitor;
import pl.edu.uj.tcs.memorytracer.agent.Constants;
import pl.edu.uj.tcs.memorytracer.agent.MemoryTracerAgent;

/**
 * @author Karol Banyś.
 */
class PreProcessingInformationMethodVisitor extends MethodVisitor {

    private String name;

    PreProcessingInformationMethodVisitor(int api, String className, String methodName, String desc, MethodVisitor mv) {
        super(api, mv);
        //name = String.format(Constants.IDENTIFYING_NAME,className,methodName,desc);
    }

    @Override
    public void visitMaxs(int maxStack, int maxLocals) {
        //MemoryTracerAgent.addMethodMaxLocal(name, maxLocals);
        //MemoryTracerAgent.addMethodMaxStack(name, maxStack);
        super.visitMaxs(maxStack, maxLocals);
    }
}
