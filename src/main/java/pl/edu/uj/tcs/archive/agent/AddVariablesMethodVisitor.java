package pl.edu.uj.tcs.archive.agent;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.LocalVariablesSorter;

import static org.objectweb.asm.Type.getMethodDescriptor;

/**
 * @author Karol Banyś.
 */
public class AddVariablesMethodVisitor extends LocalVariablesSorter {

    private int id;

    public AddVariablesMethodVisitor(int access, String desc, MethodVisitor methodVisitor) {
        super(access, desc, methodVisitor);
    }

    protected AddVariablesMethodVisitor(int api, int access, String desc, MethodVisitor methodVisitor) {
        super(api, access, desc, methodVisitor);
    }

    @Override
    public void visitCode() {
        super.visitCode();

        //call static method to get new id
        mv.visitMethodInsn(Opcodes.INVOKESTATIC, "pl/edu/uj/tcs/memorytracer/agent/MemoryTracerAgent", "getNextId", getMethodDescriptor(Type.LONG_TYPE), false);
        id = newLocal(Type.LONG_TYPE);
        mv.visitVarInsn(Opcodes.LSTORE, id);
    }

    int getId(){
        return id;
    }
}
