package pl.edu.uj.tcs.archive.agent;

import org.objectweb.asm.*;
import pl.edu.uj.tcs.memorytracer.agent.ExecutionInstruction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Karol on 2016-09-19.
 */
public class CountMaxMethodVisitor extends MethodVisitor {

    List<ExecutionInstruction> executionInstructions;

    CountMaxMethodVisitor(MethodVisitor mv) {
        super(Opcodes.ASM5, mv);
        executionInstructions = new ArrayList<>();
    }

    @Override
    public void visitCode() {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitCode();
            }
        });
    }

    @Override
    public void visitParameter(String name, int access) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitParameter(name, access);
            }
        });
    }

    @Override
    public void visitAttribute(Attribute attr) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitAttribute(attr);
            }
        });
    }

    public void visitFrame(int type, int nLocal, Object[] local, int nStack, Object[] stack) {
        Object[] localTemp = Arrays.copyOf(local, local.length);//new Object[local.length];
        Object[] stackTemp = Arrays.copyOf(stack, stack.length);//new Object[stack.length];
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitFrame(type, nLocal, localTemp, nStack, stackTemp);
            }
        });
    }

    @Override
    public void visitInsn(int opcode) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitInsn(opcode);
            }
        });
    }

    @Override
    public void visitIntInsn(int opcode, int operand) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitIntInsn(opcode, operand);
            }
        });
    }

    @Override
    public void visitVarInsn(int opcode, int var) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitVarInsn(opcode, var);
            }
        });
    }

    @Override
    public void visitTypeInsn(int opcode, String type) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitTypeInsn(opcode, type);
            }
        });
    }

    @Override
    public void visitFieldInsn(int opcode, String owner, String name, String desc) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitFieldInsn(opcode, owner, name, desc);
            }
        });
    }

    @Override
    public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitMethodInsn(opcode, owner, name, desc, itf);
            }
        });
    }

    @Override
    public void visitMethodInsn(int opcode, String owner, String name, String desc) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitMethodInsn(opcode, owner, name, desc);
            }
        });
    }

    @Override
    public void visitInvokeDynamicInsn(String name, String desc, Handle bsm, Object... bsmArgs) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitInvokeDynamicInsn(name, desc, bsm, bsmArgs);
            }
        });
    }

    @Override
    public void visitJumpInsn(int opcode, Label label) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitJumpInsn(opcode, label);
            }
        });
    }

    @Override
    public void visitLabel(Label label) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitLabel(label);
            }
        });
    }

    @Override
    public void visitLdcInsn(Object cst) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitLdcInsn(cst);
            }
        });
    }

    @Override
    public void visitIincInsn(int var, int increment) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitIincInsn(var, increment);
            }
        });
    }

    @Override
    public void visitTableSwitchInsn(int min, int max, Label dflt, Label... labels) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitTableSwitchInsn(min, max, dflt, labels);
            }
        });
    }

    @Override
    public void visitLookupSwitchInsn(Label dflt, int[] keys, Label[] labels) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitLookupSwitchInsn(dflt, keys, labels);
            }
        });
    }

    @Override
    public void visitMultiANewArrayInsn(String desc, int dims) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitMultiANewArrayInsn(desc, dims);
            }
        });
    }

    @Override
    public void visitTryCatchBlock(Label start, Label end, Label handler, String type) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitTryCatchBlock(start, end, handler, type);
            }
        });
    }

    @Override
    public void visitLocalVariable(String name, String desc, String signature, Label start, Label end, int index) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitLocalVariable(name, desc, signature, start, end, index);
            }
        });
    }

    @Override
    public void visitLineNumber(int line, Label start) {
        executionInstructions.add(new ExecutionInstruction() {
            @Override
            public void executeInstruction() {
                mv.visitLineNumber(line, start);
            }
        });
    }

    @Override
    public void visitMaxs(int maxStack, int maxLocals) {
        /*MemoryMapperMethodVisitor memoryMapperMethodVisitor = (MemoryMapperMethodVisitor) mv;
        memoryMapperMethodVisitor.maxLocals = maxLocals;
        memoryMapperMethodVisitor.maxStack =  maxStack;*/
        for (ExecutionInstruction executionInstruction : executionInstructions) {
            executionInstruction.executeInstruction();
        }
        mv.visitMaxs(maxStack, maxLocals);
    }

    @Override
    public void visitEnd() {
        super.visitEnd();
    }
}
