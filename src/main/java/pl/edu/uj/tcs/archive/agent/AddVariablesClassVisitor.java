package pl.edu.uj.tcs.archive.agent;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 * @author Karol Banyś.
 */
public class AddVariablesClassVisitor extends ClassVisitor {


    public AddVariablesClassVisitor(int api) {
        super(api);
    }

    public AddVariablesClassVisitor(int api, ClassVisitor cv){
        super(api, cv);
    }


    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        MethodVisitor mv = cv.visitMethod(access, name, desc, signature, exceptions);
        return new AddVariablesMethodVisitor(Opcodes.ASM5, access, desc, mv);
    }
}
