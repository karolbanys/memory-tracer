package pl.edu.uj.tcs.memorytracerruntime.agent;

/**
 * @author Karol Banyś.
 */
public class Constants {

    /** Multi-level debugging option of showing information in runtime. */
    static final Boolean AGENT_DEBUG_RUNTIME = false;

    static final String IDENTIFYING_NAME = "%s/%s%s";
    static final String UNDEFINED_FROM_EXCEPTION = "Ask for log edge with undefined from!";

    /** Array of thread names not to run instrumented code */
    static final String[] NOT_INSTRUMENT_THREADS = {"Finalizer", "Reference Handler"};
}
