package pl.edu.uj.tcs.memorytracerruntime.logger;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * @author Karol Banyś.
 */
public class TraceLogger {

    /**
     * Path for file with logs.
     */
    private Path filePath = null;

    /**
     * Set filePath for Path of pathName.
     * Deleting file if exist and then create file under specified path.
     * @param pathName - name of path under which file should be created.
     */
    private Path setFilePath(String pathName){
        if (pathName == null){
            pathName = Constants.DEFAULT_PATH;
        }

        filePath = Paths.get(pathName);
	
        if (filePath.getParent() != null){
            try {
                Files.createDirectories(filePath.getParent());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        try {
            Files.deleteIfExists(filePath);
            Files.createFile(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Create Path: " + pathName);
        return filePath;
    }

    public TraceLogger(){
        setFilePath(null);
    }

    public TraceLogger(String name) {
        setFilePath(name);
    }

    public boolean writeAddEdge(long from, long to){
        return writeData(String.format(Constants.ADD_EDGE_FORMAT, from, to));
    }

    public boolean writeRemoveEdge(long from, long to){
        return writeData(String.format(Constants.REMOVE_EDGE_FORMAT, from, to));
    }

    private synchronized boolean writeData(String data){
        try (OutputStream out = new BufferedOutputStream(Files.newOutputStream(filePath, StandardOpenOption.WRITE, StandardOpenOption.APPEND))){
            out.write(data.getBytes(), 0, data.getBytes().length);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean writeByteArray(byte[] data){
        try (OutputStream out = new BufferedOutputStream(Files.newOutputStream(filePath, StandardOpenOption.WRITE, StandardOpenOption.APPEND))){
            out.write(data, 0, data.length);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
