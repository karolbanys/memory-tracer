package pl.edu.uj.tcs.memorytracerruntime.logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Karol Banyś.
 */
public class Checker {

    private Map<Integer, Vertex> vertexMap;

    private Set<Vertex> reachableVertexes;
    private Set<Vertex> visited;

    private Path filePath = null;

    private Checker(String pathName){
        System.out.println("Check:"+pathName);
        vertexMap = new HashMap<>();
        reachableVertexes = new HashSet<>();
        filePath = Paths.get(pathName);
        makeVertexReachable(0);
    }

    private Vertex getVertex(int number) {
        if (vertexMap.containsKey(number)) {
            return vertexMap.get(number);
        }
        Vertex v = new Vertex(number);
        vertexMap.put(number, v);
        return v;
    }

    private void makeVertexReachable(int number) {
        Vertex v = getVertex(number);
        v.reachable();
        reachableVertexes.add(v);
    }

    private void check(){
        int errorCounter = 0;
        Set<Integer> errorVertexes = new HashSet<>();
        try (InputStream in = Files.newInputStream(filePath);
             BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
            String line;
            int lineNumber = 0;
            while ((line = reader.readLine()) != null) {
                try {
                    lineNumber++;
                    System.out.print(line);
                    Pattern addRemoveEdgePattern = Pattern.compile(Constants.ADD_REMOVE_EDGE_REGEX);
                    Matcher lineMatcher = addRemoveEdgePattern.matcher(line);
                    if (lineMatcher.find()) {
                        int fromVertexNumber = Integer.parseInt(lineMatcher.group(2));
                        int toVertexNumber = Integer.parseInt(lineMatcher.group(3));
                        if (Constants.ADD_PREFIX.equals(lineMatcher.group(1))) {
                            addEdge(fromVertexNumber, toVertexNumber);
                        } else if (Constants.REMOVE_PREFIX.equals(lineMatcher.group(1))) {
                            removeEdge(fromVertexNumber, toVertexNumber);
                        }

                    } else {
                        throw new CheckException(Constants.WRONG_LINE_EXCEPTION, line);
                    }
                    System.out.println(" (OK)");
                } catch (CheckException e) {
                    errorCounter++;
                    System.out.println(String.format(Constants.MESSAGE_EXCEPTION_IN_LINE, lineNumber, e.toString()));
                    if (e.exceptionArgs[0] instanceof Integer) {
                        errorVertexes.add((Integer) e.exceptionArgs[0]);
                    }
                    if (Constants.BREAK_AT_ERROR){
                        break;
                    }
                }
            }
        } catch (IOException x) {
            x.printStackTrace();
        }
        System.out.println("Check complete!");
        if (errorCounter == 0) {
            System.out.println("Correct! :)");
        } else {
            System.out.println("Wrong! :( Error vertex(es):"+errorVertexes.size()+" in "+errorCounter+" error(s).");
        }
    }

    private void addEdge(int fromNumber, int toNumber) {
        Vertex fromVertex = getVertex(fromNumber);
        Vertex toVertex = getVertex(toNumber);
        fromVertex.addNeighbour(toVertex);
        makeVertexReachable(toNumber);
    }

    private void removeEdge(int fromNumber, int toNumber) {
        Vertex fromVertex = getVertex(fromNumber);
        Vertex toVertex = getVertex(toNumber);
        fromVertex.removeNeighbour(toVertex);
        searchFromMainRootVertex();
    }

    private void searchFromMainRootVertex() {
        visited = new HashSet<>();
        depthFirstSearch(getVertex(0));
        reachableVertexes.removeAll(visited);
        for (Vertex v : reachableVertexes) {
            v.unreachable();
        }
        reachableVertexes = visited;
    }

    private void depthFirstSearch(Vertex v) {
        visited.add(v);
        for (Vertex u : v.getAdjacency()) {
            if (!visited.contains(u)) {
                if (VertexState.UNREACHABLE.equals(u.getState()))
                    throw new CheckException(Constants.UNREACHABLE_BECAME_REACHABLE, u.getNumber());
                depthFirstSearch(u);
            }
        }
    }

    public static void main(String[] args) {
        new Checker(Constants.DEFAULT_PATH).check();
    }


}
