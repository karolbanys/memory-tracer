package pl.edu.uj.tcs.memorytracerruntime.logger;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Karol Banyś.
 */
class Vertex {
    /**
     * There are three possible states:
     * 0 - never access
     * 1 - occur and reachable from 0
     * 2 - unreachable from 0
     */
    private VertexState state;

    /**
     * Vertex number representation.
     */
    private int number;

    /**
     * Adjacency list of Vertexes.
     */
    private List<Vertex> adjacency;

    Vertex(int number) {
        this.number = number;
        this.state = VertexState.NEVER_TOUCH;
        this.adjacency = new ArrayList<>();
    }

    VertexState getState(){
        return state;
    }

    void reachable() {
        if (VertexState.UNREACHABLE.equals(state))
            throw new CheckException(Constants.UNREACHABLE_BECAME_REACHABLE, this.getNumber());
        state = VertexState.REACHABLE;
    }

    void unreachable() {
        state = VertexState.UNREACHABLE;
    }

    int getNumber(){
        return number;
    }

    void addNeighbour(Vertex to) {
        if (VertexState.NEVER_TOUCH.equals(this.getState()))
            throw new CheckException(Constants.NEVER_TOUCH_SOURCE_EXCEPTION, this.getNumber());
        if (VertexState.UNREACHABLE.equals(this.getState()))
            throw new CheckException(Constants.UNREACHABLE_SOURCE_EXCEPTION, this.getNumber());
        if (VertexState.UNREACHABLE.equals(to.getState()))
            throw  new CheckException(Constants.UNREACHABLE_TARGET_EXCEPTION, to.getNumber());
        adjacency.add(to);
    }

    void removeNeighbour(Vertex to) {
        adjacency.remove(to);
    }

    List<Vertex> getAdjacency() {
        return adjacency;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && (obj instanceof Vertex) && this.getNumber() == ((Vertex) obj).getNumber();
    }

    @Override
    public int hashCode() {
        return ((Integer)getNumber()).hashCode();
    }
}
