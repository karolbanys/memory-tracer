package pl.edu.uj.tcs.memorytracerruntime.logger;

/**
 * Representation exception while check.
 * @author Karol Banyś.
 */
class CheckException extends RuntimeException {

    Object[] exceptionArgs;

    CheckException(String formatString, Object... args){
        super(String.format(formatString, args));
        exceptionArgs = args;
    }
}
