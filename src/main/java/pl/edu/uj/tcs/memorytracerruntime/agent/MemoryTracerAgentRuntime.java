package pl.edu.uj.tcs.memorytracerruntime.agent;

import pl.edu.uj.tcs.memorytracerruntime.collections.WeakIdentityHashMap;
import pl.edu.uj.tcs.memorytracerruntime.logger.TraceLogger;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * @author Karol Banyś.
 */
public class MemoryTracerAgentRuntime {
    private static final long mainRootVertexId = 0L;
    private static TraceLogger traceLogger;
    private static long id = 1L;
    private  static WeakIdentityHashMap<Object, Long> currentAccessed;
    private static ThreadLocal<Stack<Long>> activationRecordsStack = new ThreadLocal<Stack<Long>>(){
        @Override
        protected Stack<Long> initialValue() {
            return new Stack<>();
        }
    };
    private static ThreadLocal<Long> uninitializedThisId = new ThreadLocal<Long>() {
        @Override
        protected Long initialValue() {
            return -1L;
        }
    };
    private static ThreadLocal<Integer> agentRuntime = new ThreadLocal<Integer>(){
        @Override
        protected Integer initialValue() {
            return 0;
        }
    };
    private static Set<String> instrumentedMethod;
    private static boolean logUnknownFromZero;

    static {
        currentAccessed = new WeakIdentityHashMap<>();
        instrumentedMethod = new HashSet<>();
        logUnknownFromZero = true;
    }

    public static void createTraceLogger(String logFilePath) {
        traceLogger = new TraceLogger(logFilePath);
    }

    public static boolean currentAccessedContainsKey(Object o) {
        return currentAccessed.containsKey(o);
    }

    public static void setLogUnknownFromZero(boolean b){
        logUnknownFromZero = b;
    }

    /* GET ID */

    public static long getCurrentId() {
        return id;
    }

    public static synchronized long getNextIdRT() {
        try {
            if (increaseAgentRuntime()) {
                if (Constants.AGENT_DEBUG_RUNTIME) {
                    System.out.println("[INFO] Next id:"+id);
                }
                return id++;
            }
        } finally {
            decreaseAgentRuntime();
        }
        return -2L;
    }

    private static long getNextId() {
        return id++;
    }

    private static synchronized long registerObject(Object o) {
        long new_id = getNextId();
        if (Constants.AGENT_DEBUG_RUNTIME) {
            System.out.println("[INFO] Register object "+new_id + " " + o);
        }
        connectIdWithReference(o, new_id);
        return new_id;
    }

    private static long getExistingIdOrRegister(Object o) {
        if (currentAccessed.containsKey(o))
            return currentAccessed.get(o);
        return registerObject(o);
    }

    /* UNREGISTER SEARCH */

    public static void unregisteredSearch(Object o, long prevId) {
        if (o == null || o.getClass() == null || o.getClass().getDeclaredFields() == null) {
            if (Constants.AGENT_DEBUG_RUNTIME) {
                System.out.println("[WARNING] Object:"+o+" null reference class or fields already finished.");
            }
            return;
        }
        long oId = getExistingIdOrRegister(o);
        if (Constants.AGENT_DEBUG_RUNTIME) {
            System.out.println("[INFO] Unregistered search:"+prevId + "-->" + oId + " o:" + o + " fields count:"+o.getClass().getDeclaredFields().length);
        }
        addedReference(oId, prevId);
        for (Field field : o.getClass().getDeclaredFields()) {
            if (!field.getType().isPrimitive()) {
                try {
                    field.setAccessible(true);
                    Object oField = field.get(o);
                    if (oField != null && !currentAccessed.containsKey(oField)) {
                        if (Modifier.isStatic(field.getModifiers())) {
                            unregisteredSearch(oField, 0);
                        } else {
                            unregisteredSearch(oField, oId);
                        }
                    }
                } catch (Throwable e) {
                }
            }
        }
    }

    /* UNINITIALIZED */

    public static void connectIdWithReferenceRT(Object reference, long id) {
        try {
            if (increaseAgentRuntime()) {
                if (reference == null) {
                    if (Constants.AGENT_DEBUG_RUNTIME) {
                        System.out.println("[WARNING] Tried to connect with null reference.");
                    }
                    return;
                }
                if (currentAccessed.containsKey(reference) && currentAccessed.get(reference) != id) {
                    if (Constants.AGENT_DEBUG_RUNTIME) {
                        System.out.println("[WARNING] Reference: " + reference + " exist!");
                        System.out.println("[WARNING] Current id:" + currentAccessed.get(reference) + " new id:" + id);
                    }
                    return;
                }
                if (Constants.AGENT_DEBUG_RUNTIME) {
                    System.out.println("[INFO] Connect. Id:" + id + " Reference class name:" + reference + " Current accessed:" + currentAccessed);
                }
                currentAccessed.put(reference, id);
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    private static void connectIdWithReference(Object reference, long id) {
        if (currentAccessed.containsKey(reference) && currentAccessed.get(reference) != id) {
            if (Constants.AGENT_DEBUG_RUNTIME) {
                System.err.println("[WARNING] Reference: " + reference + " exist!");
                System.err.println("[WARNING] Current id:" + currentAccessed.get(reference) + " new id:" + id);
            }
            return;
        }
        currentAccessed.put(reference, id);
    }

    public static void changeCurrentUninitializedThisRT(long newId) {
        try {
            if (increaseAgentRuntime()) {
                if (Constants.AGENT_DEBUG_RUNTIME) {
                    System.out.println("[INFO] Change curr un_this:" + newId);
                }
                uninitializedThisId.set(newId);
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    public static void resetCurrentUninitializedThisRT() {
        try {
            if (increaseAgentRuntime()) {
                if (Constants.AGENT_DEBUG_RUNTIME) {
                    System.out.println("[INFO] Reset curr un_this:" + uninitializedThisId.get());
                }
                uninitializedThisId.set(-1L);
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    public static long getUninitializedThisIdRT() {
        try {
            if (increaseAgentRuntime()) {
                if (uninitializedThisId.get() == null) {
                    uninitializedThisId.set(-1L);
                    if (Constants.AGENT_DEBUG_RUNTIME) {
                        System.out.println("[WARNING] Uninitialized thread local.");
                    }
                }
                if (uninitializedThisId.get() == -1L) {
                    long id = getNextId();
                    if (Constants.AGENT_DEBUG_RUNTIME) {
                        System.out.println("[INFO] Add not instrumented uninitialized current:"+id);
                    }
                    addedRootSetReference(id);
                    return id;
                }
                return uninitializedThisId.get();
            }
        } finally {
            decreaseAgentRuntime();
        }
        return -3;
    }

    public static void unregisterSearchedOfInitializedRT(Object reference) {
        try {
            if (increaseAgentRuntime()) {
                if (Constants.AGENT_DEBUG_RUNTIME) {
                    System.out.println("[INFO] Search for initialized not instrumented:" + reference);
                }
                unregisteredSearch(reference, 0);
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    /* LOG ADDED REFERENCE */

    public static void addedReferenceRT(Object to, Object from) {
        try {
            if (increaseAgentRuntime()) {
                if (to == null || from == null)
                    return;
                if (!currentAccessed.containsKey(from)) {
                    throw new RuntimeException(Constants.UNDEFINED_FROM_EXCEPTION);
                }
                long fromId = currentAccessed.get(from);
                if (!currentAccessed.containsKey(to)) {
                    if (logUnknownFromZero) {
                        unregisteredSearch(to, 0);
                        traceLogger.writeAddEdge(fromId, currentAccessed.get(to));
                        return;
                    }
                    unregisteredSearch(to, fromId);
                    return;
                }
                traceLogger.writeAddEdge(fromId, currentAccessed.get(to));
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    public static void addedReferenceRT(long toId, Object from) {
        try {
            if (increaseAgentRuntime()) {
                if (from == null)
                    return;
                if (!currentAccessed.containsKey(from)) {
                    throw new RuntimeException(Constants.UNDEFINED_FROM_EXCEPTION);
                }
                traceLogger.writeAddEdge(currentAccessed.get(from), toId);
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    private static void addedReference(long toId, Object from) {
        if (from == null)
            return;
        if (!currentAccessed.containsKey(from)) {
            throw new RuntimeException(Constants.UNDEFINED_FROM_EXCEPTION);
        }
        traceLogger.writeAddEdge(currentAccessed.get(from), toId);
    }

    public static void addedReferenceRT(Object to, long fromId) {
        try {
            if (increaseAgentRuntime()) {
                if (to == null)
                    return;
                if (!currentAccessed.containsKey(to)) {
                    if (logUnknownFromZero) {
                        unregisteredSearch(to, 0);
                        traceLogger.writeAddEdge(fromId, currentAccessed.get(to));
                        return;
                    }
                    unregisteredSearch(to, fromId);
                    return;
                }
                traceLogger.writeAddEdge(fromId, currentAccessed.get(to));
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    private static void addedReference(Object to, long fromId) {
        if (to == null)
            return;
        if (!currentAccessed.containsKey(to)) {
            if (logUnknownFromZero) {
                unregisteredSearch(to, 0);
                traceLogger.writeAddEdge(fromId, currentAccessed.get(to));
                return;
            }
            unregisteredSearch(to, fromId);
            return;
        }
        traceLogger.writeAddEdge(fromId, currentAccessed.get(to));
    }

    public static void addedReferenceRT(long toId, long fromId) {
        try {
            if (increaseAgentRuntime()) {
                traceLogger.writeAddEdge(fromId, toId);
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    private static void addedReference(long toId, long fromId) {
        traceLogger.writeAddEdge(fromId, toId);
    }

    /* LOG REMOVED REFERENCE */

    public static void removedReferenceRT(Object to, Object from) {
        try {
            if (increaseAgentRuntime()) {
                if (to == null || from == null)
                    return;
                if (!currentAccessed.containsKey(from)) {
                    throw new RuntimeException(Constants.UNDEFINED_FROM_EXCEPTION);
                }
                if (!currentAccessed.containsKey(to)) {
                    if (logUnknownFromZero) {
                        unregisteredSearch(to, 0);
                    }
                    return;
                }
                traceLogger.writeRemoveEdge(currentAccessed.get(from), currentAccessed.get(to));
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    public static void removedReferenceRT(long toId, Object from) {
        try {
            if (increaseAgentRuntime()) {
                if (from == null)
                    return;
                if (!currentAccessed.containsKey(from)) {
                    throw new RuntimeException(Constants.UNDEFINED_FROM_EXCEPTION);
                }
                traceLogger.writeRemoveEdge(currentAccessed.get(from), toId);
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    private static void removedReference(long toId, Object from) {
        if (from == null)
            return;
        if (!currentAccessed.containsKey(from)) {
            throw new RuntimeException(Constants.UNDEFINED_FROM_EXCEPTION);
        }
        traceLogger.writeRemoveEdge(currentAccessed.get(from), toId);
    }

    public static void removedReferenceRT(Object to, long fromId) {
        try {
            if (increaseAgentRuntime()) {
                if (to == null)
                    return;
                if (!currentAccessed.containsKey(to)) {
                    if (logUnknownFromZero) {
                        unregisteredSearch(to, 0);
                    }
                    return;
                }
                traceLogger.writeRemoveEdge(fromId, currentAccessed.get(to));
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    public static void removedReferenceRT(long toId, long fromId) {
        try {
            if (increaseAgentRuntime()) {
                traceLogger.writeRemoveEdge(fromId, toId);
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    /* LOG FROM ROOT SET */

    private static void addedRootSetReference(Object rootSetElement) {
        if (rootSetElement == null)
            return;
        if (!currentAccessed.containsKey(rootSetElement)) {
            unregisteredSearch(rootSetElement, 0);
            return;
        }
        traceLogger.writeAddEdge(mainRootVertexId, currentAccessed.get(rootSetElement));
    }

    public static void addedRootSetReferenceRT(Object rootSetElement) {
        try {
            if (increaseAgentRuntime()) {
                if (rootSetElement == null)
                    return;
                if (!currentAccessed.containsKey(rootSetElement)) {
                    unregisteredSearch(rootSetElement, 0);
                    return;
                }
                traceLogger.writeAddEdge(mainRootVertexId, currentAccessed.get(rootSetElement));
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    private static void addedRootSetReference(long rootSetElementId) {
        traceLogger.writeAddEdge(mainRootVertexId, rootSetElementId);
    }

    public static void addedRootSetReferenceRT(long rootSetElementId) {
        try {
            if (increaseAgentRuntime()) {
                traceLogger.writeAddEdge(mainRootVertexId, rootSetElementId);
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    public static void removedRootSetReferenceRT(Object rootSetElement) {
        try {
            if (increaseAgentRuntime()) {
                if (rootSetElement == null)
                    return;
                if (!currentAccessed.containsKey(rootSetElement)) {
                    unregisteredSearch(rootSetElement, 0);
                    return;
                }
                traceLogger.writeRemoveEdge(mainRootVertexId, currentAccessed.get(rootSetElement));
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    public static void removedRootSetReferenceRT(long rootSetElementId) {
        try {
            if (increaseAgentRuntime()) {
                traceLogger.writeRemoveEdge(mainRootVertexId, rootSetElementId);
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    /* ACTIVATION RECORD */

    public static long createNewActivationRecordRT() {
        try {
            if (increaseAgentRuntime()) {
                return addActivationRecordToThreadStack();
            }
        } finally {
            decreaseAgentRuntime();
        }
        return -4;
    }

    private static long addActivationRecordToThreadStack() {
        if (activationRecordsStack.get() == null) {
            activationRecordsStack.set(new Stack<>());
        }
        if(activationRecordsStack.get().empty()) {
            if (!currentAccessed.containsKey(Thread.currentThread())) {
                long threadId = registerObject(Thread.currentThread());
                if (Constants.AGENT_DEBUG_RUNTIME) {
                    System.out.println("[INFO] New thread '"+ Thread.currentThread().getName() + "' added.");
                }
                addedRootSetReference(threadId);
            }
        }
        long activationRecordId = getNextId();
        connectIdWithReference(activationRecordId, activationRecordId);
        activationRecordsStack.get().push(activationRecordId);
        if (Constants.AGENT_DEBUG_RUNTIME) {
            System.out.println("[INFO] Add AR:"+activationRecordId+".");
        }
        addedReference(activationRecordId, Thread.currentThread());
        return activationRecordId;
    }

    public static void logAddedToPreviousActivationRecordRT(Object toLog, long activationRecordId) {
        try {
            if (increaseAgentRuntime()) {
                if (activationRecordsStack.get() == null) {
                    if (Constants.AGENT_DEBUG_RUNTIME) {
                        System.out.println("[ERROR] Activation Record Stack null when rewriting references.");
                    }
                }
                Stack<Long> stackAR = activationRecordsStack.get();
                if (Constants.AGENT_DEBUG_RUNTIME) {
                    System.out.println("[INFO] Current Activation Record:"+activationRecordId);
                    //System.out.println("[INFO] Reference: "+ toLog);
                    System.out.print("[INFO] Stack:");
                    List<Long> stackCopy = new ArrayList<>(stackAR);
                    for (Long activationRecord : stackCopy) {
                        System.out.print(" " + activationRecord);
                    }
                    System.out.println();
                }
                if (stackAR.size() <= 1 || stackAR.peek() != activationRecordId) {
                    if (Constants.AGENT_DEBUG_RUNTIME) {
                        System.out.println("[WARNING] Try rewrite value for last Activation Record:"+activationRecordId+" in Thread:"+Thread.currentThread().getName()+".");
                        addedRootSetReference(toLog);
                    }
                    return;
                }
                long prevAR = stackAR.get(stackAR.size() - 2);
                addedReference(toLog, prevAR);
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    public static void deleteActivationRecordRT(long activationRecordId) {
        try {
            if (increaseAgentRuntime()) {
                if (activationRecordsStack.get().empty() || activationRecordsStack.get().peek() != activationRecordId) {
                    if (Constants.AGENT_DEBUG_RUNTIME) {
                        System.out.println("[WARNING] Delete NOT existing AR:"+activationRecordId+" from stack.");
                    }
                    removedReference(activationRecordId, Thread.currentThread());
                } else {
                    if (Constants.AGENT_DEBUG_RUNTIME) {
                        System.out.println("[INFO] Delete AR:"+activationRecordId+" from stack.");
                    }
                    long popAR = activationRecordsStack.get().pop();
                    removedReference(popAR, Thread.currentThread());
                }
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    /* NATIVE METHODS PROCESSING */

    public static boolean isMethodInstrumentedRT(String className, String methodName, String desc) {
        try {
            if (increaseAgentRuntime()) {
                if (Constants.AGENT_DEBUG_RUNTIME && !instrumentedMethod
                        .contains(String.format(Constants.IDENTIFYING_NAME, className, methodName, desc)))
                    System.out.println("[INFO] Not instrumented:"+String.format(Constants.IDENTIFYING_NAME, className, methodName, desc));
                return instrumentedMethod
                        .contains(String.format(Constants.IDENTIFYING_NAME, className, methodName, desc));
            }
        } finally {
            decreaseAgentRuntime();
        }
        return true;
    }

    public static void addInstrumentedMethod(String name) {
        instrumentedMethod.add(name);
    }

    public static void addReturnValueOfNotInstrumentedRT(Object o, long activationRecordId) {
        try {
            if (increaseAgentRuntime()) {
                if (!currentAccessed.containsKey(o)) {
                    addedRootSetReference(o);
                }
                addedReference(o, activationRecordId);
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    /* MEMORY_MAPPER_DEBUG_COMPILE RUNTIME INFO */

    public static void debugInfoRT(String s) {
        try {
            if (increaseAgentRuntime()) {
                System.out.println(s+" THREAD["+Thread.currentThread().getName()+"]");
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    public static void handleThrowableRT(Throwable t) {
        try {
            if (increaseAgentRuntime()) {
                System.err.println("THREAD:"+Thread.currentThread().getName());
                t.printStackTrace();
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    /* LOG CONSTANT */

    public static void addConstantRT (Object o) {
        try {
            if (increaseAgentRuntime()) {
                if (!currentAccessed.containsKey(o)) {
                    addedRootSetReference(o);
                }
            }
        } finally {
            decreaseAgentRuntime();
        }
    }

    /* AGENT RUNTIME */

    public static boolean increaseAgentRuntime() {
        if (agentRuntime.get() == null) {
            agentRuntime.set(0);
        }
        int old = agentRuntime.get();
        agentRuntime.set(old+1);
        return old == 0;
    }

    public static void decreaseAgentRuntime() {
        int old = agentRuntime.get();
        agentRuntime.set(old-1);
    }

    public static boolean isLoggingEnabledRT() {
        try{
            if(increaseAgentRuntime()) {
                for (String notInstrumentThread : Constants.NOT_INSTRUMENT_THREADS) {
                    if (Thread.currentThread().getName().equals(notInstrumentThread)) {
                        return false;
                    }
                }
                return true;
            }
        } catch (Throwable t){
            if (Constants.AGENT_DEBUG_RUNTIME) {
                System.err.println("[WARNING] IsLoggingEnabled Exception THREAD:" + Thread.currentThread().getName());
                t.printStackTrace();
            }
        } finally {
            decreaseAgentRuntime();
        }
        return false;
    }

}
