package pl.edu.uj.tcs.memorytracerruntime.logger;

/**
 * @author Karol Banyś.
 */
enum VertexState {

    /**
    * 0 - never access
    */
    NEVER_TOUCH(0),
    /**
     * 1 - occur and reachable from 0
     */
    REACHABLE(1),
    /**
     * 2 - unreachable from 0
     */
    UNREACHABLE(2);

    private int stateNumber;

    VertexState(int state) {
        stateNumber = state;
    }

    int getStateNumber(){
        return stateNumber;
    }
}
