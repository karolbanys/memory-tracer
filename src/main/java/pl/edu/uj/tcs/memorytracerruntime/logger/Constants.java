package pl.edu.uj.tcs.memorytracerruntime.logger;

import java.io.File;

/**
 * @author Karol Banyś.
 */
public final class Constants {

    /** Default path to write logs for Trace Logger */
    static final String DEFAULT_PATH = "TraceLoggerFiles"+ File.separator+"logs.txt";

    /** Format of logs */
    static final String ADD_PREFIX = "add";
    static final String REMOVE_PREFIX = "remove";
    static final String ADD_EDGE_FORMAT = ADD_PREFIX+" %d --> %d\n";
    static final String REMOVE_EDGE_FORMAT = REMOVE_PREFIX+" %d --> %d\n";
    static final String ADD_REMOVE_EDGE_REGEX = "^("+ADD_PREFIX+"|"+REMOVE_PREFIX+") (\\d*) --> (\\d*)$";

    /** Exceptions check message */
    static final String WRONG_LINE_EXCEPTION = "Wrong log line: %s";
    static final String UNREACHABLE_SOURCE_EXCEPTION = "Attempting add edge from unreachable source - %d.";
    static final String UNREACHABLE_TARGET_EXCEPTION = "Attempting add edge to unreachable target - %d.";
    static final String NEVER_TOUCH_SOURCE_EXCEPTION = "Attempting add edge from never touch source - %d.";
    static final String UNREACHABLE_BECAME_REACHABLE = "Vertex %d became reachable after being unreachable.";
    static final String MESSAGE_EXCEPTION_IN_LINE = "(ERROR)\nError at line:%d. %s";
    //static final String WRONG__EXCEPTION = "";

    /** Trigger for checker if break when got validation error */
    static final Boolean BREAK_AT_ERROR = false;

}
