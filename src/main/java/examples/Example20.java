package examples;

/**
 * @author Karol Banyś.
 */
class ExampleNot20 {
    private Object t;

    ExampleNot20(Object o) {
        t = o;
    }

    Object getT() {
        return t;
    }
}

public class Example20 {
    ExampleNot20 getExampleNot20() {
        Object o = new Object();
        return new ExampleNot20(o);
    }

    public static void main(String[] args) {
        ExampleNot20 ex = new Example20().getExampleNot20();
        System.out.println(ex.getT());
    }

}
