package examples;

/**
 * @author Karol Banyś.
 */

class A{
    A(){
        //System.out.println("A:"+this);
    }
}
class B extends A{
    B(){
        //super();
        //System.out.println("B:"+this);
    }

    B(String t) {
        System.out.println(t);
    }

    B(A a) {}

}
class C extends B{
    C(String t){
        super(new A());
    }
}

class D {
    D(A a){}
}

public class Example10 {

    public static void main(String[] args) {
       C c = new C("Ala");
       D d = new D(new A());
    }

}
