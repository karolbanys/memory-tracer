package examples;

/**
 * @author Karol Banyś.
 */
public class Example17 {

    static boolean flag = true;

    boolean isAble(int a) {
        return a > 0;
    }

    static boolean check(Example17 ex) {
        return flag && ex.isAble(10);
    }

    public static void main(String[] args) {
        int a = 2;
        boolean t = check(new Example17());

    }
}
