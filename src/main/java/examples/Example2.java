package examples;

/**
 * Created by Karol Banyś.
 */
public class Example2 {

    public void example(){
    }

    public boolean example(boolean x){
        Boolean o1 = x;
        return o1;
    }

    public int example(int x){
        Integer o1 = x;
        return o1;
    }

    public long example(long x){
        Long o1 = x;
        return o1;
    }

    public float example(float x){
        Float o1 = x;
        return o1;
    }

    public double example(double x){
        Double o1 = x;
        return o1;
    }

    public Object example(Object x){
        Object o1 = new Object();
        return o1;
    }


    public static void main(String[] args){

        new Example2().example();
        new Example2().example(true);
        new Example2().example(1);
        new Example2().example(1L);
        new Example2().example(3.2f);
        new Example2().example(3.3);
        new Example2().example(new Object());
    }

}
