package examples;

/**
 * @author Karol Banyś.
 */
public class Example4 {

    //should log empty objects
    public void example_void(){
        Object o1 = new Object();
        Object o2 = new Object();
        Object o3 = new Object();
    }

    public Object example_multiple_ref(Object x){
        System.out.println("x:"+x.hashCode());
        Object o1 = x;
        Object o2 = o1;
        Object o3 = new Object();
        return o2;
    }

    public Object example_single_ref(Object x){
        return x;
    }


    public static void main(String[] args){
        Example4 ex = new Example4();
        ex.example_void();
        ex.example_multiple_ref(new Object());
        //ex.example_multiple_ref(null);
        Object x = ex.example_single_ref(new Object());
    }
}
