package examples;

/**
 * @author Karol Banyś.
 */
class TestNotInstrumented {
    private static TestNotInstrumented t = null;

    static void createObject(){
        t = new TestNotInstrumented();
    }

    static TestNotInstrumented getT(){
        return t;
    }
}

public class Example18 {
    public static void main(String[] args) {
        TestNotInstrumented.createObject();
        System.out.println(TestNotInstrumented.getT());
        System.out.println(TestNotInstrumented.getT());
    }
}
