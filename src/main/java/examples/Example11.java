package examples;

import java.util.Random;

/**
 * @author Karol Banyś.
 */
public class Example11 {

    public static void main(String[] args) {
        try{
            if (new Random().nextInt()%2 == 0)
                throw new RuntimeException();
            return;
        } catch (Exception e) {
            //MemoryTracerAgent.getNextId();
        }
    }
}
