package examples;

/**
 * Created by Karol Banyś.
 */
public class Example1 {

    void example(){
        String a = "a";
        String b = a+1;
        a = a+1;
        System.out.println(b.equals(a));
    }

    public static void main(String[] args){
        new Example1().example();
    }
}
