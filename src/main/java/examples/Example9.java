package examples;

/**
 * @author Karol Banyś.
 */
public class Example9 {

    Integer i;
    Long l;
    Object o;

    Example9() {
        Object temp;
        i = new Integer(0);
        l = new Long(0);
        temp = new Object();
        o = temp;
    }

    public static void main(String[] args) {
        Example9 ex;
        ex = new Example9();
        ex = null;
    }
}
