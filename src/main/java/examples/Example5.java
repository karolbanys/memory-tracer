package examples;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Karol Banyś.
 */
public class Example5 {

    public void example_array(){
        Object[] o = new Object[5];
        for(int i=0; i<o.length; i++){
            Object x = o[i];
        }
    }

    public void example_arrayList(){
        List<Object> o = new ArrayList<>();
        for(Object element : o){
            Object x = element;
        }
    }

    public static void main(String[] args){
        new Example5().example_array();
        new Example5().example_arrayList();
    }
}
