package examples;

/**
 * @author Karol Banyś.
 */

class NotExample19 {
    static Example19 getExample19() {
        return new Example19();
    }
}

public class Example19 {

    public static void main(String[] args) {
        Example19 ex = new Example19();
        System.out.println(new NotExample19());
        Example19 ex2 = NotExample19.getExample19();
        System.out.println(ex);
        System.out.println(ex2);
    }
}
