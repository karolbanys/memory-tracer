package examples;

/**
 * @author Karol Banyś.
 */
public class Example8 {

    Object t = new Object();

    void example(){
        Object o = new Object();
        System.out.println("DONE");
    }

    Object exampleException() throws Exception{
        try{
            int j = 1;
            Object o = new Object();
            /*if (t != null) {
                throw new Exception();
            }*/
            Integer k = 1;
            System.out.println("Zero");
            return k;
        } catch(Exception e) {
            System.out.println(e);
            throw e;
        }finally {
            System.out.println("Finally");
        }
    }

    void exampleThrow(){
        throw new RuntimeException();
    }

    void exampleThread(){
        Thread.currentThread().getId();
    }

    public static void main(String[] args){
        new Example8().example();
    }
}
