package examples;

/**
 * @author Karol Banyś.
 */
public class Example6 {

    public Object count(Object a, Object b){
        return a;
    }

    public void example(Object a){
        Object t = count(a,count(a=new Object(), new Object()));
    }

    public static void main(String[] args){
        new Example6().example(new Object());
        new Example6().example(new Object());
    }
}
