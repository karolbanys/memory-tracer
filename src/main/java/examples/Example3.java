package examples;

/**
 * Created by Karol Banyś.
 */
public class Example3 {
    public Object O = new Object();
    public static Object O2 = new Object();

    Example3(){
        System.out.println(O);
    }

    public static void main(String[] args){
        System.out.println(O2);
        new Example3();
    }
}
