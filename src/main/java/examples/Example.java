package examples;

import java.util.Random;

/**
 * @author Karol Banyś.
 */
public class Example {

    public static void main(String[] args){
        new Example1().example();

        new Example2().example();
        new Example2().example(true);
        new Example2().example(1);
        new Example2().example(1L);
        new Example2().example(3.2f);
        new Example2().example(3.3);
        new Example2().example(new Object());

        System.out.println(Example3.O2);
        new Example3();

        Example4 ex = new Example4();
        ex.example_void();
        ex.example_multiple_ref(new Object());
        //ex.example_multiple_ref(null);
        Object x = ex.example_single_ref(new Object());

        new Example5().example_array();
        new Example5().example_arrayList();

        new Example6().example(new Object());
        new Example6().example(new Object());

        Example7 ex7 = new Example7();
        try {
            Example7.t = 1;
        } catch (RuntimeException e) {
        }

        new Example8().example();

        Example9 ex9;
        ex9 = new Example9();
        ex9 = null;

        C c = new C("Ala");
        D d = new D(new A());

        try{
            if (new Random().nextInt()%2 == 0)
                throw new RuntimeException();
        } catch (Exception e) {
            System.out.println("EX11");
        }

        long t = 1L;
        t = t--;
        long p = t;
        Example12.loop1(new int[10]);
        System.out.println(" "+t);

        Long tt = 2L;
        Long o = 0L;
        Long returned = null;
        String k = " ";
        Object fds = new Object();
        /*Example12 ex12 = new Example12();
        System.out.println(ex12.hashCode());*/
        try {
            try {
                Example13 ex13 = new Example13();
                returned = ex13.divided(tt, o);
            } catch(Throwable e) {
                System.out.println(e);
                throw e;
            }
        } catch(Throwable e) {
            System.out.println(e);
        } finally {
            System.out.println("Finally");
        }
        System.out.println("END "+returned);

        try {
            throw new RuntimeException("T");
        } catch (Exception e) {
            System.out.println(e);
        }

        Example15 ex15 = new Example15();
        ex15.example2();
        try {
            ex15.example();
        } catch (Exception e) {
            System.out.println(e);
        }

        Example16 ex16 = new Example16();
        try {
            Thread.dumpStack();
        } catch (Exception e) {
            System.out.println(e);
        }

        int a = 2;
        boolean kkkkk = Example17.check(new Example17());

        System.out.println(kkkkk);
        System.out.println("END");
    }
}
