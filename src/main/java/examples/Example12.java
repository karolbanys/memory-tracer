package examples;

/**
 * @author Karol Banyś.
 */
public class Example12 {

    static void loop1(int[] array) {
        int length = array.length;
        for (int i = 0; i < length; i++) {
            array[i] = i;
            System.out.print("ar:"+array[i]+" ");
            array[i]--;
        }
        System.out.println("END loop");
    }


    public static void main(String[] args) {
        long t = 1L;
        t = t--;
        long p = t;
        loop1(new int[10]);
        System.out.println(" "+t);
    }
}
