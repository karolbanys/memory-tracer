package examples;

/**
 * @author Karol Banyś.
 */
public class Example14 {

    public static void main(String[] args) {

        try {
            throw new RuntimeException("T");
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("END");
    }
}
