package examples;

/**
 * @author Karol Banyś.
 */
public class Example15 {
    void example(){
        int[] t = new int[10];
        int b = t[11];
        int a = 1;
        if (a == 0) {
            a=1;
        } else {
            a=2;
        }
    }

    void example2(){
        int[] t = new int[10];
        int b = t[6];
        int a = 1;
        if (a == 0) {
            a=1;
        } else {
            a=2;
        }
        System.out.println("b:"+b);
    }


    public static void main(String[] args){
        Example15 ex15 = new Example15();
        ex15.example2();
        try {
            ex15.example();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
