package testcases;

/**
 * @author Karol Banyś.
 */
public class Test5 {

    static int a;

    static boolean isPositive () {
        return a > 0;
    }

    boolean isPositive(int a) {
        return a > 0;
    }

    public static void main(String[] args) {
        for (int i=-1; i<2; i++) {
            a = i;
            System.out.print(isPositive() + " ");
        }
        Test5 e = new Test5();
        System.out.print(e.isPositive(3));
    }
}
