package testcases;

/**
 * @author Karol Banyś.
 */
class A{
    A(){
        super();
    }
}

class B extends A{
    B(){
        super();
        new A();
    }
}

class C extends B{
    C(){
        super();
    }
}

public class Test1 {
    public static void main(String[] args) {
        C c = new C();
        System.out.println(c);
    }
}
