package testcases;

/**
 * @author Karol Banyś.
 */

public class Test2 {

    Object recur(int a) {
        if (a == 0)
            return new Object();
        return recur(a-1);
    }

    public static void main(String[] args) {
        Test2 t = new Test2();
        Object r = t.recur(5);
        System.out.println(r);
    }
}
