package testcases;

/**
 * @author Karol Banyś.
 */

public class Test3{

    static void exc1(){
        throw new RuntimeException("My exception.");
    }

    static void exc2(){
        exc1();
    }

    public static void main(String[] args) {
        try {
            String s = "End.";
            try {
                exc2();
            } catch (RuntimeException e) {
                System.out.println(e.getMessage());
            }
            System.out.println(s);
        } catch (RuntimeException e) {
            System.out.println("Wrong catch.");
        }
    }
}