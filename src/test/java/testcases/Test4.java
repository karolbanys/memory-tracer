package testcases;

/**
 * @author Karol Banyś.
 */
public class Test4 {
    static Object a = new Object();
    Object b = new Object();

    public static void main(String[] args) {
        Test4 t = new Test4();
        System.out.println(a);
        System.out.println(t.b);
        a = new Object();
        t.b = new Object();
        System.out.println(a);
        System.out.println(t.b);
    }
}
